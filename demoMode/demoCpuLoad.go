package demoMode

import (
	"math/rand"
	"time"

	"gitlab.com/simiecc/go-moni-widget/data"
	"gitlab.com/simiecc/golib/clog"
)

type Stat = data.Stat
type DiskStat = data.DiskStat
type BatteryStat = data.BatteryStat

func StartLoadDetect(ticker *time.Ticker) chan Stat {
	deliverChan := make(chan Stat, 10)
	// Build first record of detection record
	clog.Info("Initializing DEMO load detection...")
	go func() {

		// 0. Initial phase
		//detectSystemDisks()
		//checkSystemPerformace(0)

		runCount := 1

		// 1. Running phase
		clog.Debug("StartLoadDetect()")
		for range ticker.C {
			//clog.Debug("Detect tick!")
			deliverChan <- checkSystemPerformace(runCount)
			runCount = (runCount + 1) % 60
		}
		clog.Debug("end of StartLoadDetect()")
	}()

	return deliverChan
}

func checkSystemPerformace(tick int) Stat {
	switch {
	case tick >= 0 && tick <= 10:
		return Stat{
			CpuLoad:         int(tick) * 10,
			MemoryLoad:      int(tick) * 10,
			MemoryTotalInMB: 8000,
			MemoryUsedInMB:  1843,
			DiskLoad: []DiskStat{
				{Name: "C:", BuzyPercent: float64(tick) * 10, FreeBytes: 100012, TotalBytes: 181921},
				{Name: "D:", BuzyPercent: float64(tick) * 10, FreeBytes: 100012, TotalBytes: 181921},
			},
			Battery: BatteryStat{
				AcLineStatus: 1,
				Charging:     false,
				Level:        int((float32(tick) / 30.0) * 100.0),
			},
		}

	case tick >= 11 && tick <= 20:
		base := 20 - tick
		return Stat{
			CpuLoad:         int(base) * 10,
			MemoryLoad:      int(base) * 10,
			MemoryTotalInMB: 8000,
			MemoryUsedInMB:  1843,
			DiskLoad: []DiskStat{
				{Name: "C:", BuzyPercent: float64(base) * 10, FreeBytes: 100012, TotalBytes: 181921},
				{Name: "D:", BuzyPercent: float64(base) * 10, FreeBytes: 100012, TotalBytes: 181921},
			},
			Battery: BatteryStat{
				AcLineStatus: 1,
				Charging:     false,
				Level:        int((float32(tick) / 30.0) * 100.0),
			},
		}

	case tick >= 21 && tick <= 30:
		base := int((tick-20)/2) * 2
		return Stat{
			CpuLoad:         int(base) * 10,
			MemoryLoad:      int(base) * 10,
			MemoryTotalInMB: 8000,
			MemoryUsedInMB:  1843,
			DiskLoad: []DiskStat{
				{Name: "C:", BuzyPercent: float64(base) * 10, FreeBytes: 100012, TotalBytes: 181921},
				{Name: "D:", BuzyPercent: float64(base) * 10, FreeBytes: 100012, TotalBytes: 181921},
			},
			Battery: BatteryStat{
				AcLineStatus: 1,
				Charging:     false,
				Level:        int((float32(tick) / 30.0) * 100.0),
			},
		}

	case tick >= 31 && tick <= 40:
		return Stat{
			CpuLoad:         100,
			MemoryLoad:      100,
			MemoryTotalInMB: 8000,
			MemoryUsedInMB:  1843,
			DiskLoad: []DiskStat{
				{Name: "C:", BuzyPercent: 100, FreeBytes: 100012, TotalBytes: 181921},
				{Name: "D:", BuzyPercent: 100, FreeBytes: 100012, TotalBytes: 181921},
			},
			Battery: BatteryStat{
				AcLineStatus: 1,
				Charging:     true,
				Level:        int((float32(60-tick) / 30.0) * 100.0),
			},
		}

	default:
		return Stat{
			CpuLoad:         int(rand.Intn(101)),
			MemoryLoad:      int(rand.Intn(101)),
			MemoryTotalInMB: 8000,
			MemoryUsedInMB:  1843,
			DiskLoad: []DiskStat{
				{Name: "C:", BuzyPercent: float64(rand.Intn(101)), FreeBytes: 100012, TotalBytes: 181921},
				{Name: "D:", BuzyPercent: float64(rand.Intn(101)), FreeBytes: 100012, TotalBytes: 181921},
			},
			Battery: BatteryStat{
				AcLineStatus: 0,
				Level:        int((float32(60-tick) / 30.0) * 100.0),
			},
		}
	}

}
