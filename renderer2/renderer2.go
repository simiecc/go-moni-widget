package renderer2

import (
	"fmt"
	"syscall"

	"github.com/simie-cc/w32"
	"gitlab.com/simiecc/go-moni-widget/config"
	"gitlab.com/simiecc/go-moni-widget/utils"
	"gitlab.com/simiecc/golib/clog"
)

const (
	storedMaxHistorySize = int(20)
	cpuBarWidth          = int32(3)
	cpuBarInnerWidth     = int32(int32(storedMaxHistorySize) * cpuBarWidth)
	cpuBarTotalWidth     = int32(cpuBarInnerWidth + 2*margin + 2*borderMargin)
	memBarWidth          = int32(40)
	diskBarWidth         = int32(30)
	margin               = int32(3)
	borderMargin         = int32(2)
	//fontName             = "Cooper Black"
	// fontName = "Segoe UI"
	// fontName = "Caladea"
	fontName = "Carlito"

	thinBarHeight     = int32(5)
	thinBarFullHeight = int32(28)
)
const runningAverageCount = 10

var (
	defaultBarColor       = w32.RGB(171, 214, 214)
	diskBarColor          = w32.RGB(211, 169, 158)
	diskBarIndicatorColor = w32.RGB(211, 74, 74)
)

type DiagramRenderer struct {
	initialized   bool
	parentDC      w32.HDC
	hdc           w32.HDC
	stats         []Stat
	latestStat    Stat
	averageStat   RunningAverageStat
	clientRect    *w32.RECT
	width, height int32
	fontHeight    int32

	cpuArea, memArea GraphTextArea
	diskArea         []GraphTextArea
	renderSem        chan int
}

type GraphTextArea struct {
	Area, GraphArea, TextArea w32.RECT
}

func NewDiagramRenderer(mainDC w32.HDC, rect *w32.RECT) *DiagramRenderer {

	renderDC := w32.CreateCompatibleDC(mainDC)
	renderer := &DiagramRenderer{
		initialized: false,
		parentDC:    mainDC,
		hdc:         renderDC,
		renderSem:   make(chan int, 1),
		clientRect:  rect,
		width:       rect.Right,
		height:      rect.Bottom,
	}
	renderer.initBitmap()
	renderer.setupFont(renderDC)

	// renderer.SetRect(clientRect)
	// renderer.clearBitmap()
	return renderer
}

func (dr *DiagramRenderer) lock() func() {
	dr.renderSem <- 1
	return func() { <-dr.renderSem }
}

func (dr *DiagramRenderer) setupFont(hdc w32.HDC) {

	hFont := w32.GetStockObject(w32.DEFAULT_GUI_FONT)
	defer w32.DeleteObject(hFont)

	var logfont w32.LOGFONT
	for index, _ := range logfont.FaceName {
		logfont.FaceName[index] = 0
	}
	str, _ := syscall.UTF16FromString(fontName)
	copy(logfont.FaceName[:], str)

	logfont.Weight = w32.FW_NORMAL
	logfont.Height = 18
	logfont.CharSet = w32.DEFAULT_CHARSET
	logfont.OutPrecision = w32.OUT_DEFAULT_PRECIS
	// logfont.Quality = w32.NONANTIALIASED_QUALITY
	logfont.Quality = w32.CLEARTYPE_QUALITY

	hNewFont := w32.CreateFontIndirect(&logfont)
	w32.SelectObject(hdc, w32.HGDIOBJ(hNewFont))

	var textMetric w32.TEXTMETRIC
	w32.GetTextMetrics(hdc, &textMetric)
	dr.fontHeight = textMetric.TmHeight

	clog.Debugf("Use font: %v (Height: %v)",
		fontName,
		textMetric.TmHeight)
}

func (dr *DiagramRenderer) Width() int32 {
	return dr.width
}

func (dr *DiagramRenderer) Height() int32 {
	return dr.height
}

func (dr *DiagramRenderer) MaxHistorySize() int {
	return storedMaxHistorySize
}

func (this *DiagramRenderer) calculateWidth() int32 {
	diskCount := int32(len(this.latestStat.DiskLoad))
	return cpuBarTotalWidth +
		memBarWidth +
		diskBarWidth*diskCount
}

func setupArea(fullArea w32.RECT, fontHeight int32) GraphTextArea {

	marginArea := w32.RECT{Top: fullArea.Top + margin, Left: fullArea.Left + margin,
		Right: fullArea.Right - margin, Bottom: fullArea.Bottom - margin}

	fontHeightAndMargin := fontHeight + 2
	return GraphTextArea{
		Area:      marginArea,
		GraphArea: w32.RECT{Top: marginArea.Top, Left: marginArea.Left, Right: marginArea.Right, Bottom: marginArea.Bottom - fontHeightAndMargin},
		TextArea:  w32.RECT{Top: marginArea.Bottom - fontHeightAndMargin, Left: marginArea.Left, Right: marginArea.Right, Bottom: marginArea.Bottom},
	}
}

func setupHorizontalArea(fullArea w32.RECT, fontHeight int32) GraphTextArea {

	// fontHeightAndMargin := fontHeight + 2
	return GraphTextArea{
		Area:      fullArea,
		GraphArea: w32.RECT{Top: fullArea.Bottom - thinBarHeight, Left: fullArea.Left, Right: fullArea.Right, Bottom: fullArea.Bottom},
		TextArea:  w32.RECT{Top: fullArea.Top, Left: fullArea.Left, Right: fullArea.Right, Bottom: fullArea.Bottom - thinBarHeight - 2},
	}
}

func (dr *DiagramRenderer) initBitmap() {

	renderBitmap := w32.CreateCompatibleBitmap(dr.parentDC, uint(dr.width), uint(dr.height))
	oldObject := w32.SelectObject(dr.hdc, w32.HGDIOBJ(renderBitmap))

	w32.DeleteObject(oldObject)
}

func (dr *DiagramRenderer) UpdateStat(stats []Stat) {
	defer dr.lock()()

	dr.initialized = true
	dr.stats = stats
	dr.latestStat = dr.stats[len(dr.stats)-1]
	dr.calculateRunningAverage()

	newWidth := dr.calculateWidth()
	if newWidth != dr.width {

		dr.width = 120
		dr.height = newWidth
		dr.clientRect = &w32.RECT{Left: 0, Top: 0, Right: int32(dr.width), Bottom: int32(dr.height)}
		clientRect := dr.clientRect

		diskCount := len(dr.latestStat.DiskLoad)

		var currentTop = clientRect.Top

		currentTop += thinBarFullHeight
		cpuFullArea := w32.RECT{Top: clientRect.Top, Left: clientRect.Left, Right: clientRect.Right, Bottom: currentTop}
		dr.cpuArea = setupHorizontalArea(cpuFullArea, dr.fontHeight)

		currentTop += thinBarFullHeight
		memFullArea := w32.RECT{Top: clientRect.Top, Left: clientRect.Left, Right: clientRect.Right, Bottom: currentTop}
		dr.memArea = setupHorizontalArea(memFullArea, dr.fontHeight)

		dr.diskArea = make([]GraphTextArea, diskCount)
		left := memFullArea.Right
		for i := 0; i < diskCount; i++ {
			currentTop += thinBarFullHeight
			diskFullArea := w32.RECT{Top: clientRect.Top, Left: clientRect.Left, Right: clientRect.Right, Bottom: currentTop}
			dr.diskArea[i] = setupHorizontalArea(diskFullArea, dr.fontHeight)

			left += diskBarWidth
		}

		dr.initBitmap()
	}
}

func (dr *DiagramRenderer) calculateRunningAverage() {
	// dr.averageStat.ResetStat()

	// // NOTE startFrom (len(this.status) - 1) and count 4 recent records
	// slen := len(dr.stats)
	// slimit := slen - 1 - runningAverageCount
	// if slimit < 0 {
	// 	slimit = 0
	// }

	// for index := slimit; index < slen; index++ {
	dr.averageStat.fadeValue()
	dr.averageStat.addTo(dr.stats[len(dr.stats)-1])
	// }

	//logger.Info("latest", dr.latestStat, "average", dr.averageStat)
}

func (dr *DiagramRenderer) RenderBitmap() {
	defer dr.lock()()

	dr.clearBitmap()

	if !dr.initialized {
		dr.renderPreparing()
	} else {
		dr.drawCPU()
		dr.drawMemory()

		for index, area := range dr.diskArea {
			diskLoad := dr.latestStat.DiskLoad[index]
			avgDiskLoad := dr.averageStat.DiskLoad[index]

			//dr.drawDiskRect(diskLoad, &area.GraphArea)
			dr.drawDiskText(diskLoad.Name, &area.TextArea)

			if diskLoad.BuzyPercent < 3 && avgDiskLoad.BuzyPercent < 3 {
				continue
			}

			diskUsedPercent := diskLoad.BuzyPercent
			dr.drawBar(&area.GraphArea, diskUsedPercent, diskBarColor)

			if avgDiskLoad.BuzyPercent >= 3 {
				dr.drawDiskRedIndicator(avgDiskLoad, &area.GraphArea)
			}
		}
	}
}

func (dr *DiagramRenderer) drawCPU() {
	drawLeftText(
		dr.hdc,
		fmt.Sprintf("%3d%%", int(dr.latestStat.CpuLoad)),
		&dr.cpuArea.TextArea)

	//dr.drawPolygenBar(&dr.cpuArea.GraphArea)
	dr.drawBar(&dr.cpuArea.GraphArea, dr.latestStat.CpuLoad, defaultBarColor)
}

func (dr *DiagramRenderer) drawMemory() {
	rect := &dr.memArea.TextArea
	usedPercent := (float32(dr.latestStat.MemoryUsedInMB) / float32(dr.latestStat.MemoryTotalInMB)) * 100.0
	drawLeftText(
		dr.hdc,
		fmt.Sprintf("%3d%%", int32(usedPercent)),
		rect)
	//drawCenterText(
	drawRightText(
		dr.hdc,
		fmt.Sprintf("%0.1f G", float64(dr.latestStat.MemoryTotalInMB-dr.latestStat.MemoryUsedInMB)/1024.0),
		rect)

	dr.drawBar(&dr.memArea.GraphArea, usedPercent, defaultBarColor)
	//dr.drawMemUsedRect(&dr.memArea.GraphArea)
}

func (dr *DiagramRenderer) RenderToDC(mainDC w32.HDC) {
	defer dr.lock()()

	w32.BitBlt(mainDC, 0, 0, int(dr.width), int(dr.height), dr.hdc, 0, 0, w32.SRCCOPY)
}

func (this *DiagramRenderer) clearBitmap() {
	// drawTransparenctBg
	if config.CurrentConfig.Interactable {
		fillBrush := w32.CreateSolidBrush(w32.RGB(220, 220, 220))
		defer w32.DeleteObject(w32.HGDIOBJ(fillBrush))

		w32.FillRect(this.hdc, this.clientRect, fillBrush)
	} else {
		transBrush := w32.CreateSolidBrush(config.TransparentColor)
		defer w32.DeleteObject(w32.HGDIOBJ(transBrush))

		w32.FillRect(this.hdc, this.clientRect, transBrush)
	}
}

func (this *DiagramRenderer) drawAreaRect() {

	hdc := this.hdc

	emptyBrush := w32.GetStockObject(w32.NULL_BRUSH)
	oldBrush := w32.SelectObject(hdc, emptyBrush)
	defer w32.SelectObject(hdc, oldBrush)

	grayPen := w32.GetStockObject(w32.DC_PEN)
	w32.SetDCPenColor(hdc, w32.RGB(128, 128, 128))
	oldPen := w32.SelectObject(hdc, w32.HGDIOBJ(grayPen))
	defer w32.SelectObject(hdc, oldPen)

	this.drawRectFromRECT(&this.cpuArea.GraphArea, true)
	this.drawRectFromRECT(&this.memArea.GraphArea, false)
	for _, area := range this.diskArea {
		this.drawRectFromRECT(&area.GraphArea, false)
	}
}

func (this *DiagramRenderer) drawRectFromRECT(rect *w32.RECT, midLine bool) {
	hdc := this.hdc
	w32.Rectangle(hdc, int(rect.Left), int(rect.Top), int(rect.Right), int(rect.Bottom))
	if midLine {
		yHalf := int((rect.Bottom - rect.Top) / 2)
		w32.MoveToEx(hdc, int(rect.Left), yHalf, nil)
		w32.LineTo(hdc, int(rect.Right), yHalf)
	}
}

func (this *DiagramRenderer) drawPolygenBar(fullRect *w32.RECT) {

	hdc := this.hdc
	rect := strinkArea(fullRect)

	// Rraw bars
	barHeight := rect.Bottom - rect.Top

	//	logger.Info("Window rect", winRect)
	//	logger.Info("Bar width", barWidth, "height", barHeight)
	// Setup pen
	var lb w32.LOGBRUSH
	lb.LbStyle = w32.BS_SOLID
	lb.LbColor = w32.RGB(0, 178, 178)
	lb.LbHatch = 0

	linePen := w32.ExtCreatePen(w32.PS_GEOMETRIC, 2, &lb, 0, nil)
	defer w32.DeleteObject(w32.HGDIOBJ(linePen))
	emptyPen := w32.HPEN(w32.GetStockObject(w32.NULL_PEN))

	oldPen := w32.SelectObject(hdc, w32.HGDIOBJ(linePen))
	defer w32.SelectObject(hdc, oldPen)

	// Setup Brush
	fillBrush := w32.CreateSolidBrush(w32.RGB(171, 214, 214))
	defer w32.DeleteObject(w32.HGDIOBJ(fillBrush))

	oldBrush := w32.SelectObject(hdc, w32.HGDIOBJ(fillBrush))
	defer w32.SelectObject(hdc, oldBrush)

	// Start drawing
	//shift := (maxSize - int32(len(this.stats))) * cpuBarWidth
	var shift int32 = 0
	var prevX, prevY int32
	for index := 0; index < len(this.stats); index++ {
		if index >= len(this.stats) {
			break
		}

		stat := this.stats[index]

		xValue := rect.Left + shift + int32(index)*cpuBarWidth
		yValue := rect.Top + int32((1.0-(stat.CpuLoad/100.0))*float32(barHeight))

		if index == 0 {
			w32.MoveToEx(hdc, int(xValue), int(yValue), nil)
		} else {
			// sequence of clock-wise, start from left-bottom corner
			w32.SelectObject(hdc, w32.HGDIOBJ(emptyPen))
			points := []w32.POINT{
				w32.POINT{X: prevX, Y: rect.Bottom},
				w32.POINT{X: prevX, Y: prevY},
				w32.POINT{X: xValue, Y: yValue},
				w32.POINT{X: xValue, Y: rect.Bottom},
			}
			w32.Polygon(hdc, points, 4)

			w32.SelectObject(hdc, w32.HGDIOBJ(linePen))
			w32.LineTo(hdc, int(xValue), int(yValue))
		}

		prevX = xValue
		prevY = yValue

		//		barRect := &w32.RECT{
		//			Left:   shift + index32*barWidth,
		//			Right:  shift + (index32+1)*barWidth,
		//			Top:    barHeight - int32((float32(stat.CurrentValue)/100)*float32(barHeight)),
		//			Bottom: winRect.Bottom}

		//		w32.FillRect(hdc, barRect, hBrush)
	}
}

func drawLeftText(hdc w32.HDC, text string, rect *w32.RECT) {
	draawText0(hdc, text, rect, w32.DT_LEFT)
}

func drawRightText(hdc w32.HDC, text string, rect *w32.RECT) {
	draawText0(hdc, text, rect, w32.DT_RIGHT)
}

func drawCenterText(hdc w32.HDC, text string, rect *w32.RECT) {
	draawText0(hdc, text, rect, w32.DT_CENTER)
}

func draawText0(hdc w32.HDC, text string, rect *w32.RECT, extraFormat uint) {
	// transBrush := w32.CreateSolidBrush(config.TransparentColor)
	// defer w32.DeleteObject(w32.HGDIOBJ(transBrush))

	// oldObj := w32.SelectObject(hdc, w32.HGDIOBJ(transBrush))
	// w32.SelectObject(hdc, oldObj)

	// Foreground
	w32.SetTextColor(hdc, w32.RGB(235, 235, 235))

	// Background
	//w32.SetBkMode(hdc, w32.TRANSPARENT)
	w32.SetBkMode(hdc, w32.OPAQUE)
	w32.SetBkColor(hdc, w32.RGB(30, 30, 30))

	// Do your text drawing
	// TODO Using DrawShadowText instend of DrawText
	w32.DrawText(hdc, text,
		-1, rect, w32.DT_SINGLELINE|w32.DT_NOCLIP|w32.DT_BOTTOM|extraFormat)
	// w32.DrawShadowText(hdc, text,
	// 	-1, rect,
	// 	w32.DWORD(w32.DT_SINGLELINE|w32.DT_NOCLIP|w32.DT_BOTTOM|extraFormat),
	// 	w32.RGB(235, 235, 235), w32.RGB(30, 30, 30),
	// 	0, 0)
	// w32.DrawText(hdc, text,
	// 	-1, rect, w32.DT_SINGLELINE|w32.DT_NOCLIP|w32.DT_BOTTOM|extraFormat)
	// Always select the old font back into the DC
	// w32.SelectObject(hdc, hOldFont)
	// w32.DeleteObject(w32.HGDIOBJ(hNewFont))
}

func (dr *DiagramRenderer) drawBar(rect *w32.RECT, percent float32, color w32.COLORREF) {
	// grayBrush := w32.CreateSolidBrush(w32.RGB(127, 127, 127))
	// defer w32.DeleteObject(w32.HGDIOBJ(grayBrush))

	// var bklineRect = w32.RECT{
	// 	Left: rect.Left, Top: rect.Bottom - 1,
	// 	Right: rect.Right, Bottom: rect.Bottom,
	// }
	// w32.FillRect(dr.hdc, &bklineRect, grayBrush)

	fillBrush := w32.CreateSolidBrush(color)
	defer w32.DeleteObject(w32.HGDIOBJ(fillBrush))

	barWidth := int32(float32(rect.Right-rect.Left)*(percent/100.0) + float32(rect.Left))
	var lineRect = w32.RECT{
		Left: rect.Left, Top: rect.Top,
		Right: rect.Left + barWidth, Bottom: rect.Bottom,
	}
	w32.FillRect(dr.hdc, &lineRect, fillBrush)
}

func (this *DiagramRenderer) drawMemUsedRect(allrect *w32.RECT) {

	hdc := this.hdc
	rect := strinkArea(allrect)

	// TODO move all creation of brush(es) & pen(s) to common function and reuse them in same draw cycle
	// Setup Brush
	fillBrush := w32.CreateSolidBrush(w32.RGB(171, 214, 214))
	defer w32.DeleteObject(w32.HGDIOBJ(fillBrush))

	usedPercent := float64(this.latestStat.MemoryUsedInMB) / float64(this.latestStat.MemoryTotalInMB)
	freePercent := 1 - usedPercent
	barRect := &w32.RECT{
		Left:   rect.Left,
		Right:  rect.Right,
		Top:    int32(float64(rect.Bottom-rect.Top)*freePercent + float64(rect.Top)),
		Bottom: rect.Bottom}

	w32.FillRect(hdc, barRect, fillBrush)

	// Setup pen
	fillBrush2 := w32.CreateSolidBrush(w32.RGB(0, 178, 178))
	defer w32.DeleteObject(w32.HGDIOBJ(fillBrush2))

	lineRect := &w32.RECT{
		Left: rect.Left, Top: barRect.Top,
		Right: rect.Right, Bottom: barRect.Top + 2,
	}
	w32.FillRect(hdc, lineRect, fillBrush2)

	msg := fmt.Sprintf("%d", int(usedPercent*100))
	w32.DrawText(hdc, msg, len(msg), barRect, w32.DT_SINGLELINE|w32.DT_CENTER|w32.DT_NOCLIP|w32.DT_BOTTOM)
}

func strinkArea(rect *w32.RECT) w32.RECT {
	return w32.RECT{Left: rect.Left + borderMargin, Top: rect.Top + borderMargin,
		Right: rect.Right - borderMargin, Bottom: rect.Bottom - borderMargin}
}

var minIdlePercent = float64(1000)

func diskRect(diskData *Partial_PerfFormattedData, allrect *w32.RECT) *w32.RECT {

	rect := strinkArea(allrect)
	idlePercent := float64((100.0 - float64(diskData.PercentDiskTime)) / 100.0)

	// Workaround: PercentDiskTime over 100% sometimes (BUT WHY?)
	if idlePercent < 0 {
		idlePercent = 0
	}

	barRect := &w32.RECT{
		Left:   rect.Left,
		Right:  rect.Right,
		Top:    int32(float64(rect.Bottom-rect.Top)*idlePercent + float64(rect.Top)),
		Bottom: rect.Bottom}

	return barRect
}

func (dr *DiagramRenderer) drawDiskRect(diskData *Partial_PerfFormattedData, allrect *w32.RECT) {

	hdc := dr.hdc
	barRect := diskRect(diskData, allrect)

	// TODO move all creation of brush(es) & pen(s) to common function and reuse them in same draw cycle

	// Setup Brush
	fillBrush := w32.CreateSolidBrush(w32.RGB(171, 214, 214))
	defer w32.DeleteObject(w32.HGDIOBJ(fillBrush))

	oldBrush := w32.SelectObject(hdc, w32.HGDIOBJ(fillBrush))
	defer w32.SelectObject(hdc, oldBrush)

	w32.FillRect(hdc, barRect, fillBrush)

	// Setup pen
	fillBrush2 := w32.CreateSolidBrush(w32.RGB(0, 178, 178))
	defer w32.DeleteObject(w32.HGDIOBJ(fillBrush2))

	lineRect := &w32.RECT{
		Left: barRect.Left, Top: barRect.Top,
		Right: barRect.Right, Bottom: barRect.Top + 2,
	}
	w32.FillRect(hdc, lineRect, fillBrush2)
}

func (dr *DiagramRenderer) drawDiskRedIndicator(diskData DiskStat, rect *w32.RECT) {

	fillBrush := w32.CreateSolidBrush(diskBarIndicatorColor)
	defer w32.DeleteObject(w32.HGDIOBJ(fillBrush))

	percent := float32(diskData.BuzyPercent)
	barWidth := int32(float32(rect.Right-rect.Left)*(percent/100.0) + float32(rect.Left))
	var lineRect = w32.RECT{
		Left: rect.Left + barWidth - 4, Top: rect.Top - 2,
		Right: rect.Left + barWidth, Bottom: rect.Bottom,
	}
	w32.FillRect(dr.hdc, &lineRect, fillBrush)

	// hdc := dr.hdc
	// barRect := diskRect(diskData, allrect)
	// // Setup pen
	// fillBrush2 := w32.CreateSolidBrush(w32.RGB(175, 35, 35))
	// defer w32.DeleteObject(w32.HGDIOBJ(fillBrush2))

	// lineRect := &w32.RECT{
	// 	Left: barRect.Left, Top: barRect.Top,
	// 	Right: barRect.Right, Bottom: barRect.Top + 2,
	// }
	// w32.FillRect(hdc, lineRect, fillBrush2)
}

func (dr *DiagramRenderer) drawDiskText(text string, rect *w32.RECT) {
	//drawCenterText(
	drawLeftText(
		dr.hdc,
		fmt.Sprintf("%s", text),
		rect)
}

func (dr *DiagramRenderer) renderPreparing() {

	drawCenterText(
		dr.hdc,
		"Initializing...",
		dr.clientRect)
}

type RunningAverageStat struct {
	Stat
}

func (ra *RunningAverageStat) ResetStat() {
	ra.CpuLoad = 0
	ra.MemoryUsedInMB = 0
	ra.MemoryTotalInMB = 0

	for index, _ := range ra.DiskLoad {
		dl := &ra.DiskLoad[index]
		dl.BuzyPercent = 0
	}
}

func (ra *RunningAverageStat) fadeValue() {
	const fadingFactor float32 = 0.6
	const threshold uint64 = 4

	ra.CpuLoad *= fadingFactor
	ra.MemoryTotalInMB = dontcare(multiplyUint64(ra.MemoryTotalInMB, fadingFactor), threshold)
	ra.MemoryUsedInMB = dontcare(multiplyUint64(ra.MemoryUsedInMB, fadingFactor), threshold)
	for index, _ := range ra.DiskLoad {
		load := &ra.DiskLoad[index]
		load.BuzyPercent = utils.DontcareFloat32(load.BuzyPercent*fadingFactor, float32(threshold))
	}
}

func (ra *RunningAverageStat) addTo(from Stat) {

	ra.CpuLoad = maxFloat32(ra.CpuLoad, from.CpuLoad)
	ra.MemoryTotalInMB = maxUint64(ra.MemoryTotalInMB, from.MemoryTotalInMB)
	ra.MemoryUsedInMB = maxUint64(ra.MemoryUsedInMB, from.MemoryUsedInMB)

	if len(ra.DiskLoad) < len(from.DiskLoad) {
		ra.DiskLoad = make([]DiskStat, len(from.DiskLoad))
	}

	for index, _ := range from.DiskLoad {
		load := &ra.DiskLoad[index]
		load.BuzyPercent = maxFloat32(load.BuzyPercent, from.DiskLoad[index].BuzyPercent)
	}
}
