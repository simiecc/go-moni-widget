package renderer2

import (
	"gitlab.com/simiecc/go-moni-widget/data"
)

type Stat = data.Stat
type DiskStat = data.DiskStat
type Partial_PerfFormattedData = data.Partial_PerfFormattedData
