#!/bin/bash

windres='windres'
type windres >/dev/null 2>&1
if [ $? -ne 0 ]; then
    if [ -x '/usr/bin/x86_64-w64-mingw32-windres' ]; then
        echo "  => found windres in '/usr/bin/x86_64-w64-mingw32-windres'"
        windres='/usr/bin/x86_64-w64-mingw32-windres'
        fi
fi

export CGO_ENABLED=0
export GOOS=windows
export GOARCH=amd64

echo "Build syso..."
GIT_HASH=`git describe --abbrev=8 --tags --always`
GO_VERSION=`go version`
GO_VERSION=${GO_VERSION/go version /}
FILE_INFO="${GIT_HASH} (${GO_VERSION})"
FILE_INFO="${FILE_INFO/\//\\\/}"
echo "File version: ${FILE_INFO}"
sed -e "s/{{ Description }}/${FILE_INFO}/" main.rc > main.fin.rc
${windres} -o $1.syso main.fin.rc
echo "Generated $1.syso."
