package data

import "github.com/simie-cc/w32"

type Renderer interface {
	MaxHistorySize() int
	RenderBitmap()
	UpdateStat(stat Stat)
	Width() int
	Height() int
	RenderToDC(mainDC w32.HDC)
	OnDestroy()
}
