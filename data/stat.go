package data

type DiskStat struct {
	Name                  string
	BuzyPercent           float64
	TotalBytes, FreeBytes uint64
}

type Stat struct {
	CpuLoad       int
	CpuLoadKernel int

	MemoryUsedInMB  uint64
	MemoryTotalInMB uint64
	MemoryLoad      int

	DiskLoad []DiskStat

	NetworkLoad []NetworkStat

	Battery BatteryStat

	Location string
}

type BatteryStat struct {
	Shown        bool
	AcLineStatus byte
	Charging     bool
	Level        int
}

type NetworkStat struct {
	Name         string
	Ssid         string
	SendKBPerSec float64
	RecvKBPerSec float64
}

type PStat *Stat

// var statDirty = false
// var mux sync.Mutex
// var _stat []Stat

// func UpdateStat(newStat Stat) {
// 	mux.Lock()
// 	defer mux.Unlock()

// 	statDirty = true

// 	_stat = append(_stat, newStat)
// }

// func TrimStat(size int) {
// 	if len(_stat) >= size {
// 		mux.Lock()
// 		defer mux.Unlock()

// 		_stat = _stat[(len(_stat) - size):]
// 	}
// }

// func GetStatAndReset() []Stat {
// 	mux.Lock()
// 	defer mux.Unlock()

// 	statDirty = false
// 	return append([]Stat(nil), _stat...)
// }

// func StatIsDirty() bool {
// 	mux.Lock()
// 	defer mux.Unlock()

// 	return statDirty
// }
