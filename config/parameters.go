package config

import (
	"flag"
)

// program parameters
var p_debug bool
var p_demo bool
var p_perfmon bool

func InitProgramArgs() {
	flag.BoolVar(&p_debug, "d", false, "Debug mode")
	flag.BoolVar(&p_demo, "demo", false, "Demo mode")
	flag.BoolVar(&p_perfmon, "perf", false, "Enable performance logging")

	flag.Parse()
}

func IsDebug() bool {
	return p_debug
}

func IsDemoMode() bool {
	return p_demo
}

func IsPerfMon() bool {
	return p_perfmon
}
