package config

import (
	"math/rand"
	"os"
	"testing"
	"time"
)

func TestSaveConfig(t *testing.T) {
	const TEST_CONFIG = "config_test.json"
	rand.Seed(time.Now().UnixNano())

	os.Remove(TEST_CONFIG)

	config := NewWidgetConfig()
	config.Version = rand.Int()

	err := config.SaveConfig(TEST_CONFIG)
	if err != nil {
		t.Error(err)
	}

	_, err = os.Stat(TEST_CONFIG)
	if err != nil {
		t.Error(err)
	}

	config2 := NewWidgetConfig()
	config2.LoadConfig(TEST_CONFIG)

	if config2.Version != config.Version {
		t.Error("Version not equal")
	}

}
