package config

import (
	"encoding/json"
	"io/ioutil"

	"github.com/simie-cc/w32"
)

var CurrentConfig = NewWidgetConfig()

type WidgetConfig struct {
	Version      int  `json:"version"`
	Interactable bool `json:"interactable"`
	X            int  `json:"x"`
	Y            int  `json:"y"`
}

func NewWidgetConfig() *WidgetConfig {
	return &WidgetConfig{
		Version:      1,
		X:            w32.CW_USEDEFAULT,
		Y:            w32.CW_USEDEFAULT,
		Interactable: true,
	}
}

func (cfg *WidgetConfig) SaveConfig(configPath string) error {
	bytes, err := json.MarshalIndent(cfg, "", "    ")
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(configPath, bytes, 0644)
	if err != nil {
		return err
	}

	return nil
}

func (cfg *WidgetConfig) LoadConfig(configPath string) error {
	bytes, err := ioutil.ReadFile(configPath)
	if err != nil {
		return err
	}

	err = json.Unmarshal(bytes, cfg)
	if err != nil {
		return err
	}

	return nil
}
