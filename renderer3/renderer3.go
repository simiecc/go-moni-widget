package renderer3

import (
	"syscall"

	"github.com/simie-cc/w32"
	"gitlab.com/simiecc/go-moni-widget/config"
	"gitlab.com/simiecc/golib/clog"
)

type DiagramRenderer struct {
	initialized bool

	// overlayDC     w32.HDC

	// averageStat RunningAverageStat
	clientRect *w32.RECT
	maxHistory int32

	backgroundBlocks []w32.RECT

	renderSem chan int

	solidBgBrush     w32.HBRUSH
	transparentBrush w32.HBRUSH

	// blackBrush w32.HBRUSH

	// whiteBrush       w32.HBRUSH

	ctx        *rcontext
	subDrawers []subDrawer
}

type GraphTextArea struct {
	Area, GraphArea, TextArea w32.RECT
}
type GraphMinibarArea struct {
	GraphTextArea
	MinibarArea w32.RECT
}
type GraphTwobarArea struct {
	GraphTextArea

	DescArea w32.RECT
	Bar2Area w32.RECT
}

func NewDiagramRenderer(mainDC w32.HDC, rect *w32.RECT) *DiagramRenderer {

	renderDC := w32.CreateCompatibleDC(mainDC)
	// overlayDC := w32.CreateCompatibleDC(mainDC)
	renderer := &DiagramRenderer{
		initialized: false,
		renderSem:   make(chan int, 1),
		clientRect:  rect,
		ctx: &rcontext{
			parentDC:   mainDC,
			hdc:        renderDC,
			baseWidth:  windowWidth,
			baseHeight: int(rect.Bottom),
		},
		maxHistory: (windowWidth / cpuBarWidth) + 1,
	}
	renderer.subDrawers = []subDrawer{
		newBatteryDrawer(),
		// newBorderDrawer(
		// 	borderPosBottom,
		// 	newBatteryDrawer()),
		newCpuDrawer(),
		newMemDrawer(),
		newDiskDrawer(),
		newBorderDrawer(borderPosTop, nil),
		newLocationDrawer(),
		newNetDrawer(),
	}
	renderer.prepareResources()
	renderer.initBitmap()
	renderer.setupFont()
	renderer.updateThinBarFullHeight()

	for i := range renderer.subDrawers {
		renderer.subDrawers[i].Init(renderer.ctx)
	}

	return renderer
}

func (dr *DiagramRenderer) prepareResources() {
	dr.solidBgBrush = w32.CreateSolidBrush(w32.RGB(220, 220, 220))
	// dr.blackBrush = w32.CreateSolidBrush(w32.RGB(30, 30, 30))
	dr.ctx.fontBgBrush = w32.CreateSolidBrush(fontBgColor)
	// dr.whiteBrush = w32.CreateSolidBrush(w32.RGB(255, 255, 255))
	dr.transparentBrush = w32.CreateSolidBrush(config.TransparentColor)
	dr.ctx.defaultBarBrush = w32.CreateSolidBrush(defaultBarColor)
	dr.ctx.warnBarBrush = w32.CreateSolidBrush(warnBarColor)
	dr.ctx.errBarBrush = w32.CreateSolidBrush(errBarColor)
	dr.ctx.polygenBarBrush = w32.CreateSolidBrush(w32.RGB(114, 48, 21))

	var lb w32.LOGBRUSH
	lb.LbStyle = w32.BS_SOLID
	//lb.LbColor = w32.RGB(0, 178, 178)
	lb.LbColor = w32.RGB(240, 240, 240)
	lb.LbHatch = 0
	dr.ctx.polygenBarPen = w32.ExtCreatePen(w32.PS_GEOMETRIC, 1, &lb, 0, nil)

	lb.LbColor = w32.RGB(240, 240, 240)
	dr.ctx.borderPen = w32.ExtCreatePen(w32.PS_GEOMETRIC|w32.PS_DASH, 1, &lb, 0, nil)

	dr.ctx.emptyPen = w32.HPEN(w32.GetStockObject(w32.NULL_PEN))
}

func (dr *DiagramRenderer) lock() func() {
	dr.renderSem <- 1
	return func() { <-dr.renderSem }
}

func (dr *DiagramRenderer) setupFont() {

	hFont := w32.GetStockObject(w32.DEFAULT_GUI_FONT)
	defer w32.DeleteObject(hFont)

	var logfont w32.LOGFONT

	setFontName(&logfont, fontNormalName)
	logfont.Weight = w32.FW_NORMAL
	// logfont.Weight = w32.FW_BOLD
	logfont.Height = fontNormalHeight
	logfont.CharSet = w32.DEFAULT_CHARSET
	logfont.OutPrecision = w32.OUT_DEFAULT_PRECIS
	// logfont.Quality = w32.NONANTIALIASED_QUALITY
	logfont.Quality = w32.CLEARTYPE_QUALITY

	hNewFont := w32.CreateFontIndirect(&logfont)
	dr.ctx.normalFont = hNewFont

	setFontName(&logfont, fontSmallName)
	logfont.Height = fontSmallHeight
	hNewFontThin := w32.CreateFontIndirect(&logfont)
	dr.ctx.thinFont = hNewFontThin

	// hNewFont2 := w32.CreateFontIndirect(&logfont)
	// w32.SelectObject(dr.overlayDC, w32.HGDIOBJ(hNewFont2))

	// Get normalFont metrics
	hdc := dr.ctx.hdc
	var textMetric w32.TEXTMETRIC
	oldFont := w32.SelectObject(hdc, w32.HGDIOBJ(dr.ctx.normalFont))
	w32.GetTextMetrics(hdc, &textMetric)
	dr.ctx.fontNormalHeight = int(textMetric.TmHeight)
	dr.ctx.fontWidth = int(textMetric.TmAveCharWidth)
	w32.SelectObject(hdc, oldFont)

	clog.Infof("Use font (normal): %v (Height: %v)",
		fontNormalName,
		dr.ctx.fontNormalHeight)

	// Get thinFont metrics
	oldFont = w32.SelectObject(hdc, w32.HGDIOBJ(dr.ctx.thinFont))
	w32.GetTextMetrics(hdc, &textMetric)
	dr.ctx.fontThinHeight = int(textMetric.TmHeight)
	w32.SelectObject(hdc, oldFont)

	clog.Infof("Use font (thin): %v (Height: %v)",
		fontSmallName,
		dr.ctx.fontThinHeight)
}

func setFontName(logfont *w32.LOGFONT, fontName string) {
	for index := range logfont.FaceName {
		logfont.FaceName[index] = 0
	}
	str, _ := syscall.UTF16FromString(fontName)
	copy(logfont.FaceName[:], str)
}

func (dr *DiagramRenderer) Width() int {
	return dr.ctx.baseWidth
}

func (dr *DiagramRenderer) Height() int {
	return dr.ctx.baseHeight
}

func (dr *DiagramRenderer) MaxHistorySize() int {
	return int(dr.maxHistory)
}

func (dr *DiagramRenderer) updateThinBarFullHeight() {
	dr.ctx.thinBarFullHeight = barHeight + dr.ctx.fontNormalHeight + innerMargin
}

func (dr *DiagramRenderer) calculateHeight() int {

	// var batteryHeight int
	// if dr.ctx.latestStat.Battery.Shown {
	// 	batteryHeight = dr.ctx.thinBarFullHeight + sectionMargin
	// } else {
	// 	batteryHeight = 0
	// }

	// totalHeight :=
	// 	// battery
	// 	batteryHeight +
	// 		// CPU
	// 		dr.ctx.thinBarFullHeight

	// var subHeights []int
	// for i := range dr.subDrawers {
	// 	subHeight := dr.subDrawers[i].Height()
	// 	totalHeight += subHeight
	// 	subHeights = append(subHeights, subHeight)
	// }

	// clog.Debugf(
	// 	"Calculate height (%d): \n"+
	// 		"  SubHeights: %v",
	// 	totalHeight,
	// 	subHeights,
	// )
	//////////////////////////////////////////

	// clientRect := dr.clientRect
	// thinBarFullHeight := dr.ctx.thinBarFullHeight

	var currentTop = sideMargin
	var blockStart = 0
	dr.backgroundBlocks = dr.backgroundBlocks[:0]

	for i := range dr.subDrawers {
		// clog.Debug("index ", i, currentTop)
		thisVisible := dr.subDrawers[i].Visible()
		if !thisVisible {
			if blockStart != currentTop {
				dr.backgroundBlocks = append(dr.backgroundBlocks, w32.RECT{
					Left: 0, Top: int32(blockStart),
					Right: int32(dr.ctx.baseWidth), Bottom: int32(currentTop),
				})
			}
		} else if i != 0 {
			currentTop += margin
		}

		dr.subDrawers[i].SetPos(0, currentTop)
		currentTop += dr.subDrawers[i].Height()

		if !thisVisible {
			blockStart = currentTop
		}
	}

	currentTop += sideMargin

	if blockStart != currentTop {
		dr.backgroundBlocks = append(dr.backgroundBlocks, w32.RECT{
			Left: 0, Top: int32(blockStart),
			Right: int32(dr.ctx.baseWidth), Bottom: int32(currentTop),
		})
	}

	return currentTop
}

func setupArea(fullArea w32.RECT, fontHeight int32) GraphTextArea {

	marginArea := w32.RECT{Top: fullArea.Top + margin, Left: fullArea.Left + margin,
		Right: fullArea.Right - margin, Bottom: fullArea.Bottom - margin}

	fontHeightAndMargin := fontHeight + 2
	return GraphTextArea{
		Area:      marginArea,
		GraphArea: w32.RECT{Top: marginArea.Top, Left: marginArea.Left, Right: marginArea.Right, Bottom: marginArea.Bottom - fontHeightAndMargin},
		TextArea:  w32.RECT{Top: marginArea.Bottom - fontHeightAndMargin, Left: marginArea.Left, Right: marginArea.Right, Bottom: marginArea.Bottom},
	}
}

func setupHorizontalArea(fullArea w32.RECT, fontHeight int32) GraphTextArea {

	// fontHeightAndMargin := fontHeight + 2
	a := GraphTextArea{}
	a.Area = fullArea

	currentTop := fullArea.Top
	a.TextArea = w32.RECT{Top: currentTop, Left: fullArea.Left, Right: fullArea.Right, Bottom: currentTop + fontHeight}
	currentTop += fontHeight
	currentTop += innerMargin

	a.GraphArea = w32.RECT{Top: currentTop, Left: fullArea.Left, Right: fullArea.Right, Bottom: currentTop + barHeight}
	currentTop += barHeight

	return a
	// return GraphTextArea{
	// 	Area:      fullArea,
	// 	TextArea:  w32.RECT{Top: fullArea.Top, Left: fullArea.Left, Right: fullArea.Right, Bottom: fullArea.Bottom - barHeight - 1},
	// 	GraphArea: w32.RECT{Top: fullArea.Bottom - barHeight, Left: fullArea.Left, Right: fullArea.Right, Bottom: fullArea.Bottom},
	// }
}
func setupMinibarArea(fullArea w32.RECT, fontHeight int32) GraphMinibarArea {

	// fontHeightAndMargin := fontHeight + 2
	a := GraphMinibarArea{}
	a.Area = fullArea

	currentTop := fullArea.Top
	a.TextArea = w32.RECT{Top: currentTop, Left: fullArea.Left, Right: fullArea.Right, Bottom: currentTop + fontHeight}
	currentTop += fontHeight
	currentTop += innerMargin

	a.GraphArea = w32.RECT{Top: currentTop, Left: fullArea.Left, Right: fullArea.Right, Bottom: currentTop + barHeight}
	currentTop += barHeight

	// currentTop++
	a.MinibarArea = w32.RECT{Top: currentTop, Left: fullArea.Left, Right: fullArea.Right, Bottom: currentTop + miniBarHeight}

	return a
}

func setupTwobarArea(fullArea w32.RECT, fontNormalHeight, fontThinHeight int32) GraphTwobarArea {

	// fontHeightAndMargin := fontHeight + 2
	a := GraphTwobarArea{}
	a.Area = fullArea
	currentTop := fullArea.Top

	var origTop int32
	origTop, currentTop = appendTop(currentTop, fontThinHeight)
	a.TextArea = w32.RECT{Top: origTop, Left: fullArea.Left, Right: fullArea.Right, Bottom: currentTop}

	origTop, currentTop = appendTop(currentTop, fontNormalHeight)
	a.DescArea = w32.RECT{Top: origTop, Left: fullArea.Left, Right: fullArea.Right, Bottom: currentTop}

	origTop, currentTop = appendTop(currentTop, barHeight)
	a.GraphArea = w32.RECT{Top: origTop, Left: fullArea.Left, Right: fullArea.Right, Bottom: currentTop}

	origTop, currentTop = appendTop(currentTop, barHeight)
	a.Bar2Area = w32.RECT{Top: origTop, Left: fullArea.Left, Right: fullArea.Right, Bottom: currentTop}
	return a
}

func appendTop(origTop, sectionHeight int32) (int32, int32) {
	return origTop, origTop + sectionHeight
}

func (dr *DiagramRenderer) initBitmap() {

	renderBitmap := w32.CreateCompatibleBitmap(dr.ctx.parentDC, uint(dr.Width()), uint(dr.Height()))
	oldObject := w32.SelectObject(dr.ctx.hdc, w32.HGDIOBJ(renderBitmap))
	w32.DeleteObject(oldObject)

	// textBitmap := w32.CreateCompatibleBitmap(dr.parentDC, uint(dr.width), uint(dr.height))
	// oldObject2 := w32.SelectObject(dr.overlayDC, w32.HGDIOBJ(textBitmap))
	// w32.DeleteObject(oldObject2)
}

func (dr *DiagramRenderer) UpdateStat(stat Stat) {
	defer dr.lock()()

	dr.initialized = true
	// dr.ctx.stats = stats
	dr.ctx.latestStat = stat //stats[len(stats)-1]
	// dr.latestStat = dr.ctx.latestStat()

	// dr.calculateRunningAverage()

	needUpdate := false
	for _, sd := range dr.subDrawers {
		calledResult := sd.Update()
		needUpdate = needUpdate || calledResult
	}

	if needUpdate {
		newHeight := dr.calculateHeight()
		clog.Infof("New window height: %d", newHeight)

		dr.ctx.baseWidth = windowWidth
		dr.ctx.baseHeight = newHeight
		dr.maxHistory = (windowWidth / cpuBarWidth) + 1
		dr.clientRect = &w32.RECT{Left: 0, Top: 0, Right: int32(dr.Width()), Bottom: int32(dr.Height())}

		dr.initBitmap()
	}
}

// func (dr *DiagramRenderer) calculateRunningAverage() {
// 	// dr.averageStat.ResetStat()

// 	// // NOTE startFrom (len(this.status) - 1) and count 4 recent records
// 	// slen := len(dr.stats)
// 	// slimit := slen - 1 - runningAverageCount
// 	// if slimit < 0 {
// 	// 	slimit = 0
// 	// }

// 	// for index := slimit; index < slen; index++ {
// 	dr.averageStat.fadeValue()
// 	dr.averageStat.addTo(dr.ctx.stats[len(dr.ctx.stats)-1])
// 	// }

// 	//logger.Info("latest", dr.latestStat, "average", dr.averageStat)
// }

func (dr *DiagramRenderer) RenderBitmap() {
	defer dr.lock()()

	dr.clearBitmap()

	if !dr.initialized {
		dr.renderPreparing()
	} else {
		dr.drawBackground()

		for i := range dr.subDrawers {
			dr.subDrawers[i].Draw()
		}
	}
}

func (dr *DiagramRenderer) drawBackground() {
	hdc := dr.ctx.hdc

	for _, r := range dr.backgroundBlocks {
		w32.FillRect(hdc, &r, dr.ctx.fontBgBrush)
	}

	// client := dr.clientRect

	// if drawBattery {
	// 	w32.FillRect(hdc, client, dr.fontBgBrush)

	// } else {
	// 	cpu := dr.cpuArea
	// 	w32.FillRect(hdc,
	// 		&w32.RECT{
	// 			// Left, Top, Right, Bottom
	// 			Left: cpu.Area.Left, Top: cpu.Area.Top, Right: client.Right, Bottom: client.Bottom,
	// 		},
	// 		dr.fontBgBrush)
	// }
	// bgRect := *dr.clientRect
	// bgRect.Left = bgRect.Right - (5 * dr.fontWidth)
	// w32.FillRect(dr.hdc, &bgRect, dr.fontBgBrush)

	//w32.FillRect(dr.hdc, &dr.memArea.TextArea, dr.polygenBarBrush)

	// bg2Rect := dr.memArea.TextArea
	// bg2Rect.Right = bg2Rect.Left + (5 * dr.fontWidth)
	// w32.FillRect(dr.hdc, &bg2Rect, dr.fontBgBrush)
}

func (dr *DiagramRenderer) RenderToDC(mainDC w32.HDC) {
	defer dr.lock()()

	if !w32.BitBlt(mainDC, 0, 0, int(dr.Width()), int(dr.Height()), dr.ctx.hdc, 0, 0, w32.SRCCOPY) {
		clog.Error("Bitblt error: ", w32.GetLastError())
		panic("Bitblt error")
	}
}

func (dr *DiagramRenderer) clearBitmap() {
	if config.CurrentConfig.Interactable {
		w32.FillRect(dr.ctx.hdc, dr.clientRect, dr.solidBgBrush)
	} else {
		w32.FillRect(dr.ctx.hdc, dr.clientRect, dr.transparentBrush)
	}

	// w32.FillRect(dr.overlayDC, dr.clientRect, dr.transparentBrush)
}

// func (dr *DiagramRenderer) drawAreaRect() {

// 	hdc := dr.ctx.hdc

// 	emptyBrush := w32.GetStockObject(w32.NULL_BRUSH)
// 	oldBrush := w32.SelectObject(hdc, emptyBrush)
// 	defer w32.SelectObject(hdc, oldBrush)

// 	grayPen := w32.GetStockObject(w32.DC_PEN)
// 	w32.SetDCPenColor(hdc, w32.RGB(128, 128, 128))
// 	oldPen := w32.SelectObject(hdc, w32.HGDIOBJ(grayPen))
// 	defer w32.SelectObject(hdc, oldPen)

// 	dr.drawRectFromRECT(&dr.cpuArea.GraphArea, true)
// 	dr.drawRectFromRECT(&dr.memArea.GraphArea, false)
// 	for _, area := range dr.diskArea {
// 		dr.drawRectFromRECT(&area.GraphArea, false)
// 	}
// }

func (dr *DiagramRenderer) drawRectFromRECT(rect *w32.RECT, midLine bool) {
	hdc := dr.ctx.hdc
	w32.Rectangle(hdc, int(rect.Left), int(rect.Top), int(rect.Right), int(rect.Bottom))
	if midLine {
		yHalf := int((rect.Bottom - rect.Top) / 2)
		w32.MoveToEx(hdc, int(rect.Left), yHalf, nil)
		w32.LineTo(hdc, int(rect.Right), yHalf)
	}
}

func drawLeftText(hdc w32.HDC, font w32.HFONT, text string, rect *w32.RECT) {
	drawText0(hdc, font, text, rect, w32.DT_LEFT)
}

func drawRightText(hdc w32.HDC, font w32.HFONT, text string, rect *w32.RECT) {
	drawText0(hdc, font, text, rect, w32.DT_RIGHT)
}

func drawCenterText(hdc w32.HDC, font w32.HFONT, text string, rect *w32.RECT) {
	drawText0(hdc, font, text, rect, w32.DT_CENTER)
}

func drawText0(hdc w32.HDC, font w32.HFONT, text string, rect *w32.RECT, extraFormat uint) {

	hOldFont := w32.SelectObject(hdc, w32.HGDIOBJ(font))

	// Foreground
	w32.SetTextColor(hdc, w32.RGB(220, 220, 220))

	// Background
	w32.SetBkMode(hdc, w32.TRANSPARENT)
	// w32.SetBkMode(hdc, w32.OPAQUE)
	// w32.SetBkColor(hdc, config.TransparentColor)

	// Do your text drawing
	// TODO Using DrawShadowText instend of DrawText
	w32.DrawText(hdc, text,
		-1, rect, w32.DT_SINGLELINE|w32.DT_NOCLIP|w32.DT_BOTTOM|extraFormat)
	// w32.DrawShadowText(hdc, text,
	// 	-1, rect,
	// 	w32.DWORD(w32.DT_SINGLELINE|w32.DT_NOCLIP|w32.DT_BOTTOM|extraFormat),
	// 	w32.RGB(235, 235, 235), w32.RGB(30, 30, 30),
	// 	0, 0)
	// w32.DrawText(hdc, text,
	// 	-1, rect, w32.DT_SINGLELINE|w32.DT_NOCLIP|w32.DT_BOTTOM|extraFormat)
	// Always select the old font back into the DC
	w32.SelectObject(hdc, hOldFont)
}

// func (dr *DiagramRenderer) drawMemUsedRect(allrect *w32.RECT) {

// 	hdc := dr.ctx.hdc
// 	rect := strinkArea(allrect)

// 	// TODO move all creation of brush(es) & pen(s) to common function and reuse them in same draw cycle
// 	// Setup Brush
// 	fillBrush := w32.CreateSolidBrush(w32.RGB(171, 214, 214))
// 	defer w32.DeleteObject(w32.HGDIOBJ(fillBrush))

// 	usedPercent := float64(dr.ctx.latestStat.MemoryUsedInMB) / float64(dr.ctx.latestStat.MemoryTotalInMB)
// 	freePercent := 1 - usedPercent
// 	barRect := &w32.RECT{
// 		Left:   rect.Left,
// 		Right:  rect.Right,
// 		Top:    int32(float64(rect.Bottom-rect.Top)*freePercent + float64(rect.Top)),
// 		Bottom: rect.Bottom}

// 	w32.FillRect(hdc, barRect, fillBrush)

// 	// Setup pen
// 	fillBrush2 := w32.CreateSolidBrush(w32.RGB(0, 178, 178))
// 	defer w32.DeleteObject(w32.HGDIOBJ(fillBrush2))

// 	lineRect := &w32.RECT{
// 		Left: rect.Left, Top: barRect.Top,
// 		Right: rect.Right, Bottom: barRect.Top + 2,
// 	}
// 	w32.FillRect(hdc, lineRect, fillBrush2)

// 	msg := fmt.Sprintf("%d", int(usedPercent*100))
// 	w32.DrawText(hdc, msg, len(msg), barRect, w32.DT_SINGLELINE|w32.DT_CENTER|w32.DT_NOCLIP|w32.DT_BOTTOM)
// }

// func strinkArea(rect *w32.RECT) w32.RECT {
// 	return w32.RECT{Left: rect.Left + borderMargin, Top: rect.Top + borderMargin,
// 		Right: rect.Right - borderMargin, Bottom: rect.Bottom - borderMargin}
// }

// func diskRect(diskData *Partial_PerfFormattedData, allrect *w32.RECT) *w32.RECT {

// 	rect := strinkArea(allrect)
// 	idlePercent := float64((100.0 - float64(diskData.PercentDiskTime)) / 100.0)

// 	// Workaround: PercentDiskTime over 100% sometimes (BUT WHY?)
// 	if idlePercent < 0 {
// 		idlePercent = 0
// 	}

// 	barRect := &w32.RECT{
// 		Left:   rect.Left,
// 		Right:  rect.Right,
// 		Top:    int32(float64(rect.Bottom-rect.Top)*idlePercent + float64(rect.Top)),
// 		Bottom: rect.Bottom}

// 	return barRect
// }

// func (dr *DiagramRenderer) drawDiskRect(diskData *Partial_PerfFormattedData, allrect *w32.RECT) {

// 	hdc := dr.hdc
// 	barRect := diskRect(diskData, allrect)

// 	// TODO move all creation of brush(es) & pen(s) to common function and reuse them in same draw cycle

// 	// Setup Brush
// 	fillBrush := w32.CreateSolidBrush(w32.RGB(171, 214, 214))
// 	defer w32.DeleteObject(w32.HGDIOBJ(fillBrush))

// 	oldBrush := w32.SelectObject(hdc, w32.HGDIOBJ(fillBrush))
// 	defer w32.SelectObject(hdc, oldBrush)

// 	w32.FillRect(hdc, barRect, fillBrush)

// 	// Setup pen
// 	fillBrush2 := w32.CreateSolidBrush(w32.RGB(0, 178, 178))
// 	defer w32.DeleteObject(w32.HGDIOBJ(fillBrush2))

// 	lineRect := &w32.RECT{
// 		Left: barRect.Left, Top: barRect.Top,
// 		Right: barRect.Right, Bottom: barRect.Top + 2,
// 	}
// 	w32.FillRect(hdc, lineRect, fillBrush2)
// }

func (dr *DiagramRenderer) renderPreparing() {
	centerRect := *dr.clientRect
	centerRect.Top = ((centerRect.Bottom - centerRect.Top) / 2) + centerRect.Top - 1
	centerRect.Bottom = centerRect.Top + int32(dr.ctx.fontNormalHeight+2)

	w32.FillRect(dr.ctx.hdc, &centerRect, dr.ctx.fontBgBrush)

	drawCenterText(
		dr.ctx.hdc, dr.ctx.normalFont,
		"Initializing...",
		&centerRect)
}

func (dr *DiagramRenderer) OnDestroy() {

	for i := range dr.subDrawers {
		drawer, ok := dr.subDrawers[i].(subDrawerDestroyable)
		if ok {
			drawer.Destroy()
		}
	}

	w32.DeleteObject(w32.HGDIOBJ(dr.solidBgBrush))
	// w32.DeleteObject(w32.HGDIOBJ(dr.blackBrush))
	// w32.DeleteObject(w32.HGDIOBJ(dr.whiteBrush))
	w32.DeleteObject(w32.HGDIOBJ(dr.transparentBrush))
	w32.DeleteObject(w32.HGDIOBJ(dr.ctx.polygenBarBrush))
	w32.DeleteObject(w32.HGDIOBJ(dr.ctx.polygenBarPen))
	w32.DeleteObject(w32.HGDIOBJ(dr.ctx.borderPen))

	w32.DeleteObject(w32.HGDIOBJ(dr.ctx.defaultBarBrush))
	w32.DeleteObject(w32.HGDIOBJ(dr.ctx.warnBarBrush))
	w32.DeleteObject(w32.HGDIOBJ(dr.ctx.errBarBrush))

	w32.DeleteObject(w32.HGDIOBJ(dr.ctx.normalFont))
	w32.DeleteObject(w32.HGDIOBJ(dr.ctx.thinFont))
}

type RunningAverageStat struct {
	Stat

	CPUFadingCount    int32
	MemoryFadingCount int32
	DiskFadingCount   []int32
}

func (ra *RunningAverageStat) ResetStat() {
	ra.CpuLoad = 0
	ra.MemoryUsedInMB = 0
	ra.MemoryTotalInMB = 0

	for index, _ := range ra.DiskLoad {
		dl := &ra.DiskLoad[index]
		dl.BuzyPercent = 0
	}
}

// var (
// 	FadingFactor       = [...]float32{0.5, 0.5, 1.0}
// 	DefaultFadingCount = int32(len(FadingFactor))
// )

// func (ra *RunningAverageStat) fadeValue() {
// 	// fading(&ra.CpuLoad, &ra.CpuFadingCount)
// 	// fading(&ra.MemoryLoad, &ra.MemoryFadingCount)

// 	for index := range ra.DiskLoad {
// 		fading(&ra.DiskLoad[index].BuzyPercent, &ra.DiskFadingCount[index])
// 	}
// 	// const fadingFactor float32 = 0.6
// 	// const threshold float32 = 10.0

// 	// ra.CpuLoad = utils.DontcareFloat32(ra.CpuLoad*fadingFactor, threshold)
// 	// ra.MemoryLoad = utils.DontcareFloat32(ra.MemoryLoad*fadingFactor, threshold)
// 	// // ra.MemoryTotalInMB = utils.Dontcare(utils.MultiplyUint64(ra.MemoryTotalInMB, fadingFactor), threshold)
// 	// // ra.MemoryUsedInMB = utils.Dontcare(utils.MultiplyUint64(ra.MemoryUsedInMB, fadingFactor), threshold)
// 	// for index, _ := range ra.DiskLoad {
// 	// 	load := &ra.DiskLoad[index]
// 	// 	load.BuzyPercent = utils.DontcareFloat32(load.BuzyPercent*fadingFactor, threshold)
// 	// }
// }

func fading(load *float32, count *int32) {
	if *count > 0 {
		*count--
		//*load = *load * FadingFactor[*count]
	} else {
		*load = 0
	}
}

// func (ra *RunningAverageStat) addTo(from Stat) {

// 	// if from.CpuLoad > ra.CpuLoad {
// 	// 	ra.CpuLoad = from.CpuLoad
// 	// 	ra.CpuFadingCount = DefaultFadingCount
// 	// }

// 	// if from.MemoryLoad > ra.MemoryLoad {
// 	// 	ra.MemoryLoad = from.MemoryLoad
// 	// 	ra.MemoryFadingCount = DefaultFadingCount
// 	// }
// 	//ra.MemoryLoad = utils.MaxFloat32(ra.MemoryLoad, from.MemoryLoad)
// 	// ra.MemoryTotalInMB = utils.MaxUint64(ra.MemoryTotalInMB, from.MemoryTotalInMB)
// 	// ra.MemoryUsedInMB = utils.MaxUint64(ra.MemoryUsedInMB, from.MemoryUsedInMB)

// 	if len(ra.DiskLoad) < len(from.DiskLoad) {
// 		ra.DiskLoad = make([]DiskStat, len(from.DiskLoad))
// 		ra.DiskFadingCount = make([]int32, len(from.DiskLoad))
// 	}

// 	for index := range from.DiskLoad {
// 		if from.DiskLoad[index].BuzyPercent > ra.DiskLoad[index].BuzyPercent {
// 			ra.DiskLoad[index].BuzyPercent = from.DiskLoad[index].BuzyPercent
// 			ra.DiskFadingCount[index] = DefaultFadingCount
// 		}
// 		// load := &ra.DiskLoad[index]
// 		// load.BuzyPercent = utils.MaxFloat32(load.BuzyPercent, from.DiskLoad[index].BuzyPercent)
// 	}
// }
