package renderer3

import (
	"strconv"

	"github.com/simie-cc/w32"
)

type netDrawer struct {
	subDrawer
	origLen int
	netArea []netRect

	ctx *rcontext
}

func newNetDrawer() *netDrawer {
	return &netDrawer{}
}

func (ndr *netDrawer) Init(ctx *rcontext) {
	ndr.ctx = ctx
}

func (ndr netDrawer) Visible() bool {
	return len(ndr.ctx.latestStat.NetworkLoad) > 0
}

func (ndr netDrawer) Width() int {
	return ndr.ctx.baseWidth
}

func (ndr netDrawer) Height() int {
	netCount := len(ndr.ctx.latestStat.NetworkLoad)
	if netCount > 0 {
		return netCount * ndr.singleHeight()
	} else {
		return 0
	}
}

func (ndr *netDrawer) Update() bool {
	// if len(ndr.ctx.stats) == 0 {
	// 	return false
	// }

	newNets := len(ndr.ctx.latestStat.NetworkLoad)
	if newNets != ndr.origLen {
		ndr.origLen = newNets
		return true
	}

	return false
}

func (ndr netDrawer) singleHeight() int {
	return ndr.ctx.thinBarFullHeight + ndr.ctx.fontThinHeight
}

func (ndr *netDrawer) SetPos(x, y int) {
	currentTop := y
	ndr.netArea = make([]netRect, ndr.origLen)
	for i := 0; i < ndr.origLen; i++ {
		beginTop := currentTop
		// currentTop += thinBarFullHeight + dr.fontNormalHeight + thinBarHeight
		currentTop += ndr.singleHeight()
		fullArea := w32.RECT{Top: int32(beginTop), Left: int32(x),
			Right: int32(ndr.ctx.baseWidth), Bottom: int32(currentTop)}
		ndr.netArea[i] = ndr.networkRect(fullArea)
	}
}

func (ndr netDrawer) networkRect(fullArea w32.RECT) netRect {
	descRect := w32.RECT{
		Left: fullArea.Left, Top: fullArea.Top,
		Right: fullArea.Right, Bottom: fullArea.Top + int32(ndr.ctx.fontThinHeight),
	}

	midPoint := (fullArea.Right - fullArea.Left) / 2
	upRect := w32.RECT{Left: midPoint + 2*margin, Top: descRect.Bottom,
		Right: fullArea.Right, Bottom: descRect.Bottom + int32(ndr.ctx.fontNormalHeight)}
	downRect := w32.RECT{Left: fullArea.Left, Top: descRect.Bottom,
		Right: midPoint - 2*margin, Bottom: descRect.Bottom + int32(ndr.ctx.fontNormalHeight)}
	upBarRect := w32.RECT{Left: upRect.Left, Top: upRect.Bottom,
		Right: upRect.Right, Bottom: upRect.Bottom + barHeight}
	downBarRect := w32.RECT{Left: downRect.Left, Top: downRect.Bottom,
		Right: downRect.Right, Bottom: downRect.Bottom + barHeight}

	return netRect{
		descRect:    descRect,
		upRect:      upRect,
		downRect:    downRect,
		upBarRect:   upBarRect,
		downBarRect: downBarRect,
	}
}

const netBarUpperKBPerSec = 1500 // 1MB/s

func (ndr *netDrawer) Draw() {
	latestStat := ndr.ctx.latestStat
	for index, area := range ndr.netArea {
		netLoad := latestStat.NetworkLoad[index]
		ndr.drawNetLoadingText(netLoad, area)

		recvPercent := int(netLoad.RecvKBPerSec) * 100 / netBarUpperKBPerSec
		if recvPercent > 100 {
			recvPercent = 100
		}
		ndr.ctx.drawBarInt(&area.downBarRect, recvPercent, ndr.ctx.defaultBarBrush)

		sendPercent := int(netLoad.SendKBPerSec) * 100 / netBarUpperKBPerSec
		if sendPercent > 100 {
			sendPercent = 100
		}
		ndr.ctx.drawBarIntRev(&area.upBarRect, sendPercent, ndr.ctx.errBarBrush)
	}
}

func (ndr *netDrawer) drawNetLoadingText(netLoad NetworkStat, area netRect) {
	hdc := ndr.ctx.hdc

	var label string
	if len(netLoad.Ssid) > 0 {
		label = netLoad.Name + " - " + netLoad.Ssid
	} else {
		label = netLoad.Name
	}

	drawLeftText(
		hdc, ndr.ctx.thinFont,
		label,
		&area.descRect)

	// 	drawRightText(
	// 		hdc, ndr.ctx.thinFont,
	// 		netLoad.Ssid,
	// 		&area.descRect)
	// }

	drawLeftText(
		hdc, ndr.ctx.thinFont,
		"Rx",
		&area.downRect)
	drawLeftText(
		hdc, ndr.ctx.thinFont,
		"Tx",
		&area.upRect)

	recvText := ndr.formatMB(netLoad.RecvKBPerSec)
	if recvText != "" {
		drawRightText(
			hdc, ndr.ctx.normalFont,
			recvText,
			&area.downRect)
	}

	sendText := ndr.formatMB(netLoad.SendKBPerSec)
	if sendText != "" {
		drawRightText(
			hdc, ndr.ctx.normalFont,
			sendText,
			&area.upRect)
	}
}

func (ndr netDrawer) formatMB(val float64) string {
	if val > 1200.0 {
		return strconv.FormatFloat(val/1000.0, 'f', 1, 32) + "M" // fmt.Sprintf("%0.1fM", val/1024.0)
	} else {
		// valr := math.Round(float64(val))
		// if valr > 0 {
		return strconv.Itoa(int(val)) + "K" // fmt.Sprintf("%2.0f", val)
		// }
		// return ""
	}
}

// func (ndr netDrawer) drawNetBorder() {
// 	oldPen := w32.SelectObject(dr.hdc, w32.HGDIOBJ(dr.borderPen))
// 	defer w32.SelectObject(dr.hdc, oldPen)

// 	firstNetArea := dr.netArea[0].Area
// 	y := int(firstNetArea.Top - halfSectionMargin)

// 	w32.MoveToEx(dr.hdc, int(firstNetArea.Left), y, nil)
// 	w32.LineTo(dr.hdc, int(firstNetArea.Right), y)
// }

type netRect struct {
	descRect               w32.RECT
	upRect, downRect       w32.RECT
	upBarRect, downBarRect w32.RECT
}
