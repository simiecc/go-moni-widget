package renderer3

type subDrawer interface {
	Init(*rcontext)
	Visible() bool
	Width() int
	Height() int
	Update() bool
	SetPos(x, y int)
	Draw()
}

type subDrawerDestroyable interface {
	Destroy()
}
