package renderer3

import (
	"fmt"

	"github.com/simie-cc/w32"
	"gitlab.com/simiecc/go-moni-widget/utils"
)

type cpuDrawer struct {
	subDrawer
	origLen int
	cpuArea GraphTextArea

	ctx *rcontext

	// historyValue []float64
	history    *utils.CircularArr
	maxHistory int
}

func newCpuDrawer() *cpuDrawer {
	return &cpuDrawer{}
}

func (cdr *cpuDrawer) Init(ctx *rcontext) {
	cdr.ctx = ctx
	cdr.maxHistory =
		(cdr.Width()-(cdr.ctx.fontWidth*4))/
			(cpuBarWidth+cpuBarWidthMargin) + 1
	cdr.history = utils.NewCircularArr(cdr.maxHistory)
}

func (cdr cpuDrawer) Visible() bool {
	return true
}

func (cdr cpuDrawer) Width() int {
	return cdr.ctx.baseWidth
}

func (cdr cpuDrawer) Height() int {
	return cdr.ctx.thinBarFullHeight
}

func (cdr *cpuDrawer) Update() bool {

	// if len(cdr.historyValue) > cdr.maxHistory {
	// 	sizeDiff := len(cdr.historyValue) - cdr.maxHistory
	// 	cdr.historyValue = cdr.historyValue[sizeDiff:]
	// }
	// cdr.historyValue = append(cdr.historyValue, cdr.currValue)
	// cdr.historyValue = append(cdr.historyValue, cdr.ctx.latestStat.CpuLoad)
	cdr.history.Put(cdr.ctx.latestStat.CpuLoad)

	return false
}

func (cdr *cpuDrawer) SetPos(x, y int) {
	fullArea := w32.RECT{
		Left: int32(x), Top: int32(y),
		Right: int32(x + cdr.Width()), Bottom: int32(y + cdr.Height())}
	cdr.cpuArea = setupHorizontalArea(fullArea, int32(cdr.ctx.fontNormalHeight))
}

func (cdr *cpuDrawer) Draw() {
	//w32.FillRect(dr.ctx.hdc, &dr.cpuArea.TextArea, dr.fontBgBrush)
	// cdr.drawPolygenBar()
	cdr.drawBargraph()

	cpuText := fmt.Sprintf(" %d%%", int(cdr.ctx.latestStat.CpuLoad))
	drawRightText(
		cdr.ctx.hdc, cdr.ctx.normalFont,
		cpuText,
		&cdr.cpuArea.TextArea)

	//w32.BitBlt(dr.hdc, 0, 0, int(dr.width), int(dr.height), dr.overlayDC, 0, 0, w32.SRCCOPY)

	// w32.SetTextColor(dr.overlayDC, w32.RGB(0, 0, 0))
	// w32.SetBkMode(dr.overlayDC, w32.TRANSPARENT)
	// w32.DrawText(dr.overlayDC, cpuText,
	// 	-1, &dr.cpuArea.TextArea, w32.DT_SINGLELINE|w32.DT_NOCLIP|w32.DT_BOTTOM|w32.DT_RIGHT)
	// w32.BitBlt(dr.hdc, 0, 0, int(dr.width), int(dr.height), dr.overlayDC, 0, 0, w32.SRCAND)

	cdr.ctx.drawBarInt(&cdr.cpuArea.GraphArea, cdr.ctx.latestStat.CpuLoad, cdr.ctx.defaultBarBrush)
	cdr.ctx.drawBarInt(&cdr.cpuArea.GraphArea, cdr.ctx.latestStat.CpuLoadKernel, cdr.ctx.errBarBrush)
}

func (cdr *cpuDrawer) drawBargraph() {
	hdc := cdr.ctx.hdc
	rect := &cdr.cpuArea.TextArea
	barHeight := rect.Bottom - rect.Top

	// oldBrush := w32.SelectObject(hdc, w32.HGDIOBJ(cdr.ctx.polygenBarBrush))
	// defer w32.SelectObject(hdc, oldBrush)
	// for i := len(cdr.historyValue) - 1; i >= 0; i-- {
	hislen := cdr.history.Len()
	for i := 0; i < hislen; i++ {
		// currValue := cdr.historyValue[i]
		currValue, _ := cdr.history.Get(i)
		if currValue < 5.0 {
			continue
		}

		currPos := i * (cpuBarWidth + cpuBarWidthMargin)
		yValue := rect.Top + int32((1.0-(float64(currValue)/100.0))*float64(barHeight))
		barRect := w32.RECT{
			Left: int32(currPos) - cpuBarWidth, Top: yValue,
			Right: int32(currPos), Bottom: rect.Bottom,
		}
		w32.FillRect(hdc, &barRect, cdr.ctx.defaultBarBrush)
	}
}

// func (cdr *cpuDrawer) drawPolygenBar() {
// 	hdc := cdr.ctx.hdc
// 	rect := &cdr.cpuArea.TextArea

// 	//rect := strinkArea(fullRect)

// 	// Rraw bars
// 	barHeight := rect.Bottom - rect.Top

// 	//	logger.Info("Window rect", winRect)
// 	//	logger.Info("Bar width", barWidth, "height", barHeight)
// 	// Setup pen

// 	oldPen := w32.SelectObject(hdc, w32.HGDIOBJ(cdr.ctx.polygenBarPen))
// 	defer w32.SelectObject(hdc, oldPen)

// 	// Setup Brush
// 	// fillBrush := w32.CreateSolidBrush(w32.RGB(171, 214, 214))
// 	// defer w32.DeleteObject(w32.HGDIOBJ(fillBrush))

// 	oldBrush := w32.SelectObject(hdc, w32.HGDIOBJ(cdr.ctx.polygenBarBrush))
// 	defer w32.SelectObject(hdc, oldBrush)

// 	// Start drawing
// 	//shift := (maxSize - int32(len(this.stats))) * cpuBarWidth
// 	var shift int32 = 0
// 	var prevX, prevY int32
// 	for index := 0; index < len(cdr.ctx.stats); index++ {
// 		if index >= len(cdr.ctx.stats) {
// 			break
// 		}

// 		stat := cdr.ctx.stats[index]

// 		xValue := rect.Left + shift + int32(index)*cpuBarWidth
// 		yValue := rect.Top + int32((1.0-(stat.CpuLoad/100.0))*float32(barHeight))

// 		if index == 0 {
// 			w32.MoveToEx(hdc, int(xValue), int(yValue), nil)
// 		} else {
// 			// sequence of clock-wise, start from left-bottom corner
// 			w32.SelectObject(hdc, w32.HGDIOBJ(cdr.ctx.emptyPen))
// 			points := []w32.POINT{
// 				w32.POINT{X: prevX, Y: rect.Bottom},
// 				w32.POINT{X: prevX, Y: prevY},
// 				w32.POINT{X: xValue, Y: yValue},
// 				w32.POINT{X: xValue, Y: rect.Bottom},
// 			}
// 			w32.Polygon(hdc, points, 4)

// 			w32.SelectObject(hdc, w32.HGDIOBJ(cdr.ctx.polygenBarPen))
// 			w32.LineTo(hdc, int(xValue), int(yValue))
// 		}

// 		prevX = xValue
// 		prevY = yValue
// 	}
// }
