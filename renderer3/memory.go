package renderer3

import (
	"fmt"

	"github.com/simie-cc/w32"
)

type memDrawer struct {
	subDrawer
	origLen int
	memArea GraphTextArea

	ctx *rcontext
}

func newMemDrawer() *memDrawer {
	return &memDrawer{}
}

func (mdr *memDrawer) Init(ctx *rcontext) {
	mdr.ctx = ctx
}

func (mdr memDrawer) Visible() bool {
	return true
}

func (mdr memDrawer) Width() int {
	return mdr.ctx.baseWidth
}

func (mdr memDrawer) Height() int {
	return mdr.ctx.thinBarFullHeight
}

func (mdr *memDrawer) Update() bool {
	return false
}

func (mdr *memDrawer) SetPos(x, y int) {
	memFullArea := w32.RECT{
		Left: int32(x), Top: int32(y),
		Right: int32(x + mdr.Width()), Bottom: int32(y + mdr.Height())}
	mdr.memArea = setupHorizontalArea(memFullArea, int32(mdr.ctx.fontNormalHeight))
}

func (mdr *memDrawer) Draw() {
	// rect := &mdr.memArea.TextArea
	usedPercent := mdr.ctx.latestStat.MemoryLoad
	// w32.FillRect(mdr.ctx.hdc, &mdr.memArea.TextArea, mdr.ctx.fontBgBrush)
	// drawRightText(
	// 	dr.hdc, dr.normalFont,
	// 	fmt.Sprintf("%2.0f%%", usedPercent),
	// 	rect)
	// drawLeftText(
	drawLeftText(
		mdr.ctx.hdc, mdr.ctx.thinFont,
		fmt.Sprintf("%0.1f G",
			float64(mdr.ctx.latestStat.MemoryTotalInMB-mdr.ctx.latestStat.MemoryUsedInMB)/1024.0),
		&mdr.memArea.GraphArea)
	drawRightText(
		mdr.ctx.hdc, mdr.ctx.normalFont,
		fmt.Sprintf("%d%%", usedPercent),
		&mdr.memArea.TextArea)

	mdr.ctx.drawBarInt(&mdr.memArea.GraphArea, usedPercent, mdr.ctx.defaultBarBrush)
	//dr.drawMemUsedRect(&dr.memArea.GraphArea)
}
