package renderer3

import (
	"github.com/simie-cc/w32"
)

type locationDrawer struct {
	subDrawer
	area w32.RECT

	ctx *rcontext
}

func newLocationDrawer() *locationDrawer {
	return &locationDrawer{}
}

func (dr *locationDrawer) Init(ctx *rcontext) {
	dr.ctx = ctx
}

func (dr locationDrawer) Visible() bool {
	return true
}

func (lodr locationDrawer) Width() int {
	return lodr.ctx.baseWidth
}

func (lodr locationDrawer) Height() int {
	return lodr.ctx.fontNormalHeight
}

func (lodr *locationDrawer) Update() bool {
	return false
}

func (lodr *locationDrawer) SetPos(x, y int) {
	// currentTop += thinBarFullHeight
	y += margin
	lodr.area = w32.RECT{
		Left: int32(x), Top: int32(y),
		Right: int32(x + lodr.Width()), Bottom: int32(y + lodr.ctx.fontNormalHeight)}
}

func (lodr *locationDrawer) Draw() {
	if !lodr.Visible() {
		return
	}

	drawLeftText(lodr.ctx.hdc, lodr.ctx.normalFont, lodr.ctx.latestStat.Location, &lodr.area)
}

// func (bdr *locationDrawer) drawBatteryBorder() {
// 	oldPen := w32.SelectObject(bdr.ctx.hdc, w32.HGDIOBJ(bdr.ctx.borderPen))
// 	defer w32.SelectObject(bdr.ctx.hdc, oldPen)

// 	fullArea := bdr.area.Area
// 	y := int(fullArea.Bottom + halfSectionMargin)

// 	w32.MoveToEx(bdr.ctx.hdc, int(fullArea.Left), y, nil)
// 	w32.LineTo(bdr.ctx.hdc, int(fullArea.Right), y)
// }
