package renderer3

import (
	"github.com/simie-cc/w32"
)

type borderPos int

const (
	borderPosTop borderPos = iota
	borderPosBottom
)

type borderDrawer struct {
	subDrawer
	x, y int
	pos  borderPos

	ctx   *rcontext
	child subDrawer
}

func newBorderDrawer(pos borderPos, child subDrawer) *borderDrawer {
	return &borderDrawer{
		pos:   pos,
		child: child,
	}
}

func (bdr *borderDrawer) Init(ctx *rcontext) {
	bdr.ctx = ctx
	if bdr.child != nil {
		bdr.child.Init(ctx)
	}
}

func (bdr *borderDrawer) Visible() bool {
	if bdr.child != nil {
		return bdr.child.Visible()
	}
	return true
}

func (bdr *borderDrawer) Width() int {
	if bdr.child != nil {
		return bdr.child.Width()
	}
	return bdr.ctx.baseWidth
}

func (bdr *borderDrawer) Height() int {
	height := sectionMargin
	if bdr.child != nil {
		height += bdr.child.Height() + margin
	}
	return height
}

func (bdr *borderDrawer) Update() bool {
	if bdr.child != nil {
		return bdr.child.Update()
	}
	return false
}

func (bdr *borderDrawer) SetPos(x, y int) {
	bdr.x = x
	bdr.y = y + halfSectionMargin

	if bdr.child != nil {
		if bdr.pos == borderPosTop {
			bdr.child.SetPos(x, y+sectionMargin+innerMargin)
		} else {
			bdr.child.SetPos(x, y)
			bdr.y = y + bdr.child.Height() + innerMargin + halfSectionMargin
		}
	}
}

func (bdr *borderDrawer) Draw() {
	if bdr.child != nil && !bdr.child.Visible() {
		return
	}

	oldPen := w32.SelectObject(bdr.ctx.hdc, w32.HGDIOBJ(bdr.ctx.borderPen))
	defer w32.SelectObject(bdr.ctx.hdc, oldPen)

	w32.MoveToEx(bdr.ctx.hdc, bdr.x, bdr.y, nil)
	w32.LineTo(bdr.ctx.hdc, bdr.Width(), bdr.y)

	if bdr.child != nil {
		bdr.child.Draw()
	}
}
