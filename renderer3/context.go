package renderer3

import "github.com/simie-cc/w32"

type rcontext struct {
	baseWidth  int
	baseHeight int

	// stats      []Stat
	latestStat Stat

	fontNormalHeight int
	fontThinHeight   int
	fontWidth        int

	thinBarFullHeight int

	parentDC w32.HDC
	hdc      w32.HDC

	borderPen     w32.HPEN
	polygenBarPen w32.HPEN
	emptyPen      w32.HPEN

	polygenBarBrush w32.HBRUSH
	defaultBarBrush w32.HBRUSH
	warnBarBrush    w32.HBRUSH
	errBarBrush     w32.HBRUSH
	fontBgBrush     w32.HBRUSH

	normalFont w32.HFONT
	thinFont   w32.HFONT
}

func (ctx rcontext) drawBar(rect *w32.RECT, percent float32, brush w32.HBRUSH) {
	// grayBrush := w32.CreateSolidBrush(w32.RGB(127, 127, 127))
	// defer w32.DeleteObject(w32.HGDIOBJ(grayBrush))

	// var bklineRect = w32.RECT{
	// 	Left: rect.Left, Top: rect.Bottom - 1,
	// 	Right: rect.Right, Bottom: rect.Bottom,
	// }
	// w32.FillRect(dr.hdc, &bklineRect, grayBrush)

	barRemainWidth := int32(float32(rect.Right-rect.Left)*(1.0-percent/100.0) + float32(rect.Left))
	var lineRect = w32.RECT{
		Left: rect.Left + barRemainWidth, Top: rect.Top,
		Right: rect.Right, Bottom: rect.Bottom,
	}
	w32.FillRect(ctx.hdc, &lineRect, brush)
}

func (ctx rcontext) drawBar64(rect *w32.RECT, percent float64, brush w32.HBRUSH) {
	// grayBrush := w32.CreateSolidBrush(w32.RGB(127, 127, 127))
	// defer w32.DeleteObject(w32.HGDIOBJ(grayBrush))

	// var bklineRect = w32.RECT{
	// 	Left: rect.Left, Top: rect.Bottom - 1,
	// 	Right: rect.Right, Bottom: rect.Bottom,
	// }
	// w32.FillRect(dr.hdc, &bklineRect, grayBrush)

	barRemainWidth := int32(float64(rect.Right-rect.Left)*(1.0-percent/100.0) + float64(rect.Left))
	var lineRect = w32.RECT{
		Left: rect.Left + barRemainWidth, Top: rect.Top,
		Right: rect.Right, Bottom: rect.Bottom,
	}
	w32.FillRect(ctx.hdc, &lineRect, brush)
}

func (ctx rcontext) drawBarInt(rect *w32.RECT, percent int, brush w32.HBRUSH) {
	// grayBrush := w32.CreateSolidBrush(w32.RGB(127, 127, 127))
	// defer w32.DeleteObject(w32.HGDIOBJ(grayBrush))

	// var bklineRect = w32.RECT{
	// 	Left: rect.Left, Top: rect.Bottom - 1,
	// 	Right: rect.Right, Bottom: rect.Bottom,
	// }
	// w32.FillRect(dr.hdc, &bklineRect, grayBrush)

	barRemainWidth := int32(int(rect.Right-rect.Left) * (100 - percent) / 100)
	var lineRect = w32.RECT{
		Left: rect.Left + barRemainWidth, Top: rect.Top,
		Right: rect.Right, Bottom: rect.Bottom,
	}
	w32.FillRect(ctx.hdc, &lineRect, brush)
}

func (ctx rcontext) drawBarIntRev(rect *w32.RECT, percent int, brush w32.HBRUSH) {
	// grayBrush := w32.CreateSolidBrush(w32.RGB(127, 127, 127))
	// defer w32.DeleteObject(w32.HGDIOBJ(grayBrush))

	// var bklineRect = w32.RECT{
	// 	Left: rect.Left, Top: rect.Bottom - 1,
	// 	Right: rect.Right, Bottom: rect.Bottom,
	// }
	// w32.FillRect(dr.hdc, &bklineRect, grayBrush)

	barRemainWidth := int32(int(rect.Right-rect.Left) * (percent) / 100)
	var lineRect = w32.RECT{
		Left: rect.Left, Top: rect.Top,
		Right: rect.Left + barRemainWidth, Bottom: rect.Bottom,
	}
	w32.FillRect(ctx.hdc, &lineRect, brush)
}
