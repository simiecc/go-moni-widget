package renderer3

import "github.com/simie-cc/w32"

const (
	cpuBarWidth       = 5
	cpuBarWidthMargin = 0
	// memBarWidth  = int32(40)
	// diskBarWidth = int32(30)
	sideMargin  = 5
	margin      = 3
	innerMargin = 1
	// borderMargin = 3

	sectionMargin     = 3
	halfSectionMargin = 1
	//fontNormalName             = "Cooper Black"
	// fontNormalName = "Segoe UI"
	// fontNormalName = "Caladea"
	//fontNormalName = "Carlito"
	//fontNormalName = "Nirmala UI"
	fontNormalName   = "Segoe UI Black"
	fontNormalHeight = 16
	fontSmallName    = "Segoe UI"
	fontSmallHeight  = 14

	barHeight     = 8
	miniBarHeight = 3
	//thinBarFullHeight = int32(28)
	windowWidth = 120 //80
)
const runningAverageCount = 10

var (
	defaultBarColor = w32.RGB(171, 214, 214) // ABD6D6
	warnBarColor    = w32.RGB(209, 194, 35)  // D1C223 //w32.RGB(211, 169, 158)
	errBarColor     = w32.RGB(211, 74, 74)
	fontBgColor     = w32.RGB(91, 91, 91)
)
