package renderer3

import (
	"fmt"

	"github.com/simie-cc/w32"
)

type diskDrawer struct {
	subDrawer
	origLen int
	rect    []GraphMinibarArea
	thres   map[string]*diskLoadThres

	ctx *rcontext
}

func newDiskDrawer() *diskDrawer {
	return &diskDrawer{
		thres: make(map[string]*diskLoadThres),
	}
}

func (ddr *diskDrawer) Init(ctx *rcontext) {
	ddr.ctx = ctx
}

func (ddr diskDrawer) Visible() bool {
	return true
}

func (ddr diskDrawer) Width() int {
	return ddr.ctx.baseWidth
}

func (ddr diskDrawer) Height() int {
	count := len(ddr.ctx.latestStat.DiskLoad)
	if count > 0 {
		return count * ddr.singleHeight()
	} else {
		return 0
	}
}

func (ddr *diskDrawer) Update() bool {
	ddr.tickDownThres()
	// if len(ddr.ctx.stats) == 0 {
	// 	return false
	// }

	ddr.updateThres()

	newNets := len(ddr.ctx.latestStat.DiskLoad)
	if newNets != ddr.origLen {
		ddr.origLen = newNets
		return true
	}

	return false
}

func (ddr *diskDrawer) tickDownThres() {
	for key := range ddr.thres {
		m := ddr.thres[key]
		m.Tick--

		if ddr.thres[key].Tick < 0 {
			delete(ddr.thres, key)
		}
	}
}

func (ddr *diskDrawer) updateThres() {
	for _, disk := range ddr.ctx.latestStat.DiskLoad {
		if disk.BuzyPercent < 5 {
			continue
		}

		m, ok := ddr.thres[disk.Name]
		if !ok {
			ddr.thres[disk.Name] = &diskLoadThres{
				Tick:        3,
				BuzyPercent: disk.BuzyPercent,
			}
		} else if disk.BuzyPercent > m.BuzyPercent {
			m.BuzyPercent = float64(disk.BuzyPercent)
			m.Tick = 3
		}
	}
}

func (ddr diskDrawer) singleHeight() int {
	return ddr.ctx.thinBarFullHeight + miniBarHeight
}

func (ddr *diskDrawer) SetPos(x, y int) {
	currentTop := y
	ddr.rect = make([]GraphMinibarArea, ddr.origLen)
	for i := 0; i < ddr.origLen; i++ {
		beginTop := currentTop
		// currentTop += thinBarFullHeight + dr.fontNormalHeight + thinBarHeight
		currentTop += ddr.singleHeight()
		fullArea := w32.RECT{Top: int32(beginTop), Left: int32(x),
			Right: int32(ddr.ctx.baseWidth), Bottom: int32(currentTop)}
		ddr.rect[i] = setupMinibarArea(fullArea, int32(ddr.ctx.fontNormalHeight))
	}
}

func (ddr *diskDrawer) Draw() {
	for index, area := range ddr.rect {
		diskLoad := ddr.ctx.latestStat.DiskLoad[index]
		thresLoad, thresExists := ddr.thres[diskLoad.Name]
		// avgDiskLoad := ddr.averageStat.DiskLoad[index]

		diskSpaceUsedPercent := int((diskLoad.TotalBytes - diskLoad.FreeBytes) * 100 / diskLoad.TotalBytes)

		//dr.drawDiskRect(diskLoad, &area.GraphArea)
		//w32.FillRect(dr.hdc, &area.TextArea, dr.fontBgBrush)
		ddr.drawDiskText(diskLoad.Name, int(diskLoad.BuzyPercent), diskSpaceUsedPercent, area)

		// if diskLoad.BuzyPercent < 3 && avgDiskLoad.BuzyPercent < 3 {
		// 	continue
		// }

		ddr.ctx.drawBar64(&area.GraphArea, diskLoad.BuzyPercent, ddr.ctx.warnBarBrush)
		ddr.ctx.drawBarInt(&area.MinibarArea, diskSpaceUsedPercent, ddr.ctx.defaultBarBrush)

		if thresExists {
			ddr.drawDiskRedIndicator(thresLoad, &area.GraphArea)
		}
	}
}

func (ddr *diskDrawer) drawDiskRedIndicator(diskData *diskLoadThres, rect *w32.RECT) {
	percent := diskData.BuzyPercent
	// percent := 0.5
	barRemainWidth := int32(float64(rect.Right-rect.Left)*((100.0-percent)/100.0) + float64(rect.Left))
	var lineRect = w32.RECT{
		Left: rect.Left + barRemainWidth, Top: rect.Top,
		Right: rect.Left + barRemainWidth - 4, Bottom: rect.Bottom + miniBarHeight,
	}
	w32.FillRect(ddr.ctx.hdc, &lineRect, ddr.ctx.errBarBrush)
}

func (ddr *diskDrawer) drawDiskText(text string, diskBuzyPercent, diskUsedPercent int, area GraphMinibarArea) {
	drawCenterText(
		ddr.ctx.hdc, ddr.ctx.normalFont,
		fmt.Sprintf(" %s", text),
		&area.TextArea)
	drawRightText(
		ddr.ctx.hdc, ddr.ctx.normalFont,
		// fmt.Sprintf("%.0f%%", diskUsedPercent),
		fmt.Sprintf("%d%%", diskBuzyPercent),
		&area.TextArea)
	drawLeftText(
		ddr.ctx.hdc, ddr.ctx.thinFont,
		fmt.Sprintf("%d%%", diskUsedPercent),
		&area.MinibarArea)
}

type diskLoadThres struct {
	BuzyPercent float64
	Tick        int
}
