package renderer3

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"unsafe"

	"github.com/simie-cc/w32"
	"gitlab.com/simiecc/go-moni-widget/data"
	"gitlab.com/simiecc/golib/clog"
)

type batteryDrawer struct {
	subDrawer
	batteryArea              GraphTextArea
	lastVisible              bool
	charingAnimationProgress int

	ctx *rcontext

	batteryDC                   w32.HDC
	batteryWidth, batteryHeight int
	plugDC                      w32.HDC
	plugWidth, plugHeight       int
}

func newBatteryDrawer() *batteryDrawer {
	return &batteryDrawer{
		charingAnimationProgress: 0,
	}
}

func (bdr *batteryDrawer) Init(ctx *rcontext) {
	bdr.ctx = ctx

	bdr.prepareBatteryBitmap()
}

func (bdr batteryDrawer) Visible() bool {
	return bdr.lastVisible
}

func (bdr batteryDrawer) Width() int {
	return bdr.ctx.baseWidth
}

func (bdr batteryDrawer) Height() int {
	// battery := bdr.ctx.latestStat.Battery
	// if battery.AcLineStatus == 1 && !battery.Charging {
	// 	return bdr.plugHeight
	// }

	return bdr.ctx.thinBarFullHeight
}

func (bdr batteryDrawer) judgeVisible() bool {
	battery := bdr.ctx.latestStat.Battery
	return battery.Shown

	// var drawBattery = true

	// if !battery.Charging && battery.AcLineStatus == 1 {
	// 	drawBattery = false
	// }

	// return drawBattery
}

func (bdr *batteryDrawer) Update() bool {
	newVisible := bdr.judgeVisible()
	if newVisible != bdr.lastVisible {
		bdr.lastVisible = newVisible
		return true
	}

	return false
}

func (bdr *batteryDrawer) SetPos(x, y int) {
	// currentTop += thinBarFullHeight
	batteryFullArea := w32.RECT{
		Left: int32(x), Top: int32(y),
		Right: int32(x + bdr.Width()), Bottom: int32(y + bdr.Height())}
	bdr.batteryArea = setupHorizontalArea(batteryFullArea, int32(bdr.ctx.fontNormalHeight))
}

func (bdr *batteryDrawer) Draw() {
	if !bdr.Visible() {
		return
	}

	//dr.drawPolygenBar(dr.hdc, &dr.cpuArea.TextArea)
	battery := bdr.ctx.latestStat.Battery
	//w32.FillRect(bdr.ctx.hdc, &bdr.batteryArea.TextArea, bdr.ctx.fontBgBrush)
	bdr.drawBatteryBitmap(battery.AcLineStatus)

	if battery.Charging {
		bdr.ctx.drawBar64(&bdr.batteryArea.GraphArea, float64(battery.Level), bdr.ctx.defaultBarBrush)
		bdr.charingProgressAnimation()
	} else {
		if battery.AcLineStatus == 1 {
			return
		}

		if battery.Level < 30 {
			bdr.ctx.drawBar64(&bdr.batteryArea.GraphArea, float64(battery.Level), bdr.ctx.errBarBrush)
		} else {
			bdr.ctx.drawBar64(&bdr.batteryArea.GraphArea, float64(battery.Level), bdr.ctx.warnBarBrush)
		}
	}

	// text := fmt.Sprintf("%d%%", int(battery.Level))
	drawRightText(
		bdr.ctx.hdc, bdr.ctx.normalFont,
		strconv.Itoa(battery.Level)+"%",
		&bdr.batteryArea.TextArea)
	// bdr.drawBatteryBorder()
}

func (bdr *batteryDrawer) drawBatteryBitmap(acLineStatus byte) {
	area := bdr.batteryArea.TextArea

	if acLineStatus == 1 {
		w32.BitBlt(bdr.ctx.hdc,
			int(area.Left)+2, int(area.Top)+1, bdr.plugWidth, bdr.plugHeight,
			bdr.plugDC, 0, 0, w32.SRCPAINT)
	} else {
		w32.BitBlt(bdr.ctx.hdc,
			int(area.Left)+2, int(area.Top)+1, bdr.batteryWidth, bdr.batteryHeight,
			bdr.batteryDC, 0, 0, w32.SRCPAINT)
	}
}

func (bdr *batteryDrawer) drawBatteryBorder() {
	oldPen := w32.SelectObject(bdr.ctx.hdc, w32.HGDIOBJ(bdr.ctx.borderPen))
	defer w32.SelectObject(bdr.ctx.hdc, oldPen)

	fullArea := bdr.batteryArea.Area
	y := int(fullArea.Bottom + halfSectionMargin)

	w32.MoveToEx(bdr.ctx.hdc, int(fullArea.Left), y, nil)
	w32.LineTo(bdr.ctx.hdc, int(fullArea.Right), y)
}

func (bdr *batteryDrawer) charingProgressAnimation() {
	bdr.charingAnimationProgress++
	if bdr.charingAnimationProgress > 5 {
		bdr.charingAnimationProgress = 0
	}

	rect := bdr.batteryArea.GraphArea
	percent := bdr.charingAnimationProgress*(100-bdr.ctx.latestStat.Battery.Level)/5 +
		bdr.ctx.latestStat.Battery.Level
	barRemainWidth := int32(
		int(rect.Right-rect.Left)*(100-percent)/100.0 +
			int(rect.Left))
	var lineRect = w32.RECT{
		Left: barRemainWidth - 2, Top: rect.Top,
		Right: barRemainWidth + 2, Bottom: rect.Bottom,
	}
	w32.FillRect(bdr.ctx.hdc, &lineRect, bdr.ctx.defaultBarBrush)
}

func (bdr *batteryDrawer) prepareBatteryBitmap() {
	bdr.batteryDC, bdr.batteryWidth, bdr.batteryHeight = bdr.readBitmap("assets/battery5.bmp")
	bdr.plugDC, bdr.plugWidth, bdr.plugHeight = bdr.readBitmap("assets/plug1.bmp")
}

func (bdr *batteryDrawer) readBitmap(filepath string) (w32.HDC, int, int) {

	f, err := data.Static.Open(filepath)
	if err != nil {
		panic("Open image error")
	}
	defer f.Close()

	bytes, err := ioutil.ReadAll(f)
	if err != nil {
		panic("Load image error")
	}

	bfh := *(*w32.BITMAPFILEHEADER)(unsafe.Pointer(&bytes[0]))
	bih := *(*w32.BITMAPV5HEADER)(unsafe.Pointer(&bytes[unsafe.Sizeof(bfh)]))

	if bih.BiSize != uint32(unsafe.Sizeof(bih)) {
		panic(fmt.Sprintf("Size mismatch: %d", bih.BiSize))
	}

	var ppvBits unsafe.Pointer
	batteryBitmap := w32.CreateDIBSection(0, &bih.BITMAPINFOHEADER, w32.DIB_RGB_COLORS, &ppvBits, 0, 0)
	clog.Debug("hBitmap created ", batteryBitmap)
	if batteryBitmap == 0 {
		panic("Battery bitmap initialization failed")
	}

	pPixels := bytes[bfh.GetBfOffBits():]
	ret := w32.SetDIBits(0, batteryBitmap, 0, uint32(bih.BiHeight), pPixels, &bih.BITMAPINFOHEADER, w32.DIB_RGB_COLORS)
	if ret == 0 {
		panic("Battery bitmap SetDIBits failed")
	}

	// Get & Cal new W/H
	var cBitmap w32.BITMAP
	w32.GetObject(w32.HGDIOBJ(batteryBitmap), unsafe.Sizeof(cBitmap), unsafe.Pointer(&cBitmap))
	// desiredHeight := float32(dr.ctx.fontNormalHeight) * 1.0
	// dr.ctx.batteryHeight = int(desiredHeight)
	// dr.ctx.batteryWidth = int(desiredHeight*(float32(cBitmap.BmWidth)/float32(cBitmap.BmHeight))) + 1
	imgWidth := int(cBitmap.BmWidth)
	imgHeight := int(cBitmap.BmHeight)
	clog.Infof("Battery size %d x %d", imgWidth, imgHeight)

	// Target temp DC
	bitmapDC := w32.CreateCompatibleDC(bdr.ctx.parentDC)
	//batteryScaledBitmap := w32.CreateCompatibleBitmap(dr.parentDC, uint(dr.ctx.batteryWidth), uint(dr.ctx.batteryHeight))
	//oldObject := w32.SelectObject(dr.ctx.batteryDC, w32.HGDIOBJ(batteryScaledBitmap))
	//w32.DeleteObject(oldObject)
	//w32.SetStretchBltMode(dr.ctx.batteryDC, w32.COLORONCOLOR)

	// SRC DC
	//hdcMem := w32.CreateCompatibleDC(dr.parentDC)
	// oldBitmap := w32.SelectObject(hdcMem, w32.HGDIOBJ(batteryBitmap))
	oldBitmap := w32.SelectObject(bitmapDC, w32.HGDIOBJ(batteryBitmap))
	w32.DeleteObject(w32.HGDIOBJ(oldBitmap))

	return bitmapDC, imgWidth, imgHeight
	// w32.StretchBlt(
	// 	dr.ctx.batteryDC, 0, 0, dr.ctx.batteryWidth, dr.ctx.batteryHeight,
	// 	hdcMem, 0, 0, int(cBitmap.BmWidth), int(cBitmap.BmHeight),
	// 	w32.SRCCOPY)
	// w32.SelectObject(hdcMem, oldBitmap)
	// w32.DeleteDC(hdcMem)
	// w32.DeleteObject(w32.HGDIOBJ(batteryBitmap))
}

func (bdr *batteryDrawer) Destroy() {
	w32.DeleteObject(w32.HGDIOBJ(bdr.batteryDC))
	w32.DeleteObject(w32.HGDIOBJ(bdr.plugDC))
}
