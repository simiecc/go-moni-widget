package renderer3

import (
	"gitlab.com/simiecc/go-moni-widget/data"
)

type Stat = data.Stat
type DiskStat = data.DiskStat
type NetworkStat = data.NetworkStat
