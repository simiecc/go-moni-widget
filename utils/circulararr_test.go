package utils_test

import (
	"testing"

	"gitlab.com/simiecc/go-moni-widget/utils"
)

func TestNew(t *testing.T) {
	arr := utils.NewCircularArr(10)
	if arr == nil {
		t.Fail()
	}
}

func TestPut(t *testing.T) {
	arr := utils.NewCircularArr(5)
	arr.Put(1.0)

	check(t, arr, []float64{0, 0, 0, 0, 1.0})
}

func TestPut2(t *testing.T) {
	arr := utils.NewCircularArr(5)
	arr.Put(1.0)
	arr.Put(2.0)

	check(t, arr, []float64{0, 0, 0, 1.0, 2.0})
}

func TestPut6(t *testing.T) {
	arr := utils.NewCircularArr(5)
	for i := 0; i <= 6; i++ {
		arr.Put(float64(i))
	}

	check(t, arr, []float64{2.0, 3.0, 4.0, 5.0, 6.0})
}

func check(t *testing.T, arr *utils.CircularArr, compare []float64) {
	results := getAll(arr)
	if len(results) != len(compare) {
		t.Error("Length not equals")
	}
	for l := 0; l < len(results); l++ {
		if results[l] != compare[l] {
			t.Error("Item ", l, " value not equals: ", results[l], compare[l])
		}
	}
}

func getAll(arr *utils.CircularArr) []float64 {
	datas := make([]float64, 0, arr.Len())
	for i := 0; i < arr.Len(); i++ {
		val, _ := arr.Get(i)
		datas = append(datas, val)
	}
	return datas
}
