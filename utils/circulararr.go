package utils

import "errors"

type CircularArr struct {
	data  []int
	start int
	len   int
}

func NewCircularArr(len int) *CircularArr {
	return &CircularArr{
		len:   len,
		start: 0,
		data:  make([]int, len),
	}
}

func (c CircularArr) Len() int {
	return c.len
}

func (c CircularArr) Get(index int) (int, error) {
	if index > c.len {
		return 0, errors.New("OutOfBound")
	}

	index += c.start
	if index >= c.len {
		index -= c.len
	}
	return c.data[index], nil
}

func (c *CircularArr) Put(data int) {
	c.data[c.start] = data
	c.start++
	if c.start >= c.len {
		c.start = 0
	}
}
