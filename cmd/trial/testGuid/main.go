package main

import (
	"fmt"
)

type GUID struct {
	Data1 uint32
	Data2 uint16
	Data3 uint16
	Data4 [8]byte
}

func GUIDToString(guid GUID) string {
	return fmt.Sprintf("%08X-%04X-%04X-%04X-%08X",
		guid.Data1, guid.Data2, guid.Data3, guid.Data4[0:4], guid.Data4[5:])
}

func main() {
	fmt.Println(
		GUIDToString(
			GUID{1, 2, 3, [8]byte{4, 5, 6, 7, 8, 9, 10, 11}}),
	)
}
