package main

import (
	"fmt"
	"time"

	"github.com/StackExchange/wmi"
	"github.com/simie-cc/w32"
	"gitlab.com/simiecc/go-moni-widget/sysmetrics"
)

func main() {
	for {
		fmt.Print("Waiting..")
		ret := w32.NotifyAddrChange(nil, nil)
		fmt.Println(" -- Ret ", ret)
		queryNetworkInterfaces()
		time.Sleep(1 * time.Second)
	}
}

func queryNetworkInterfaces() {
	queryString := fmt.Sprintf("SELECT * FROM Win32_NetworkAdapter")
	var resultRaw []sysmetrics.MSFT_NetAdapter
	wmi.Query(queryString, &resultRaw)

	for _, r := range resultRaw {
		fmt.Println("-- Interface", r.InterfaceIndex, r.Name, r.InterfaceDescription)
	}

	var connProfile []sysmetrics.Partial_MSFT_NetConnectionProfile
	wmi.Query("SELECT * FROM MSFT_NetConnectionProfile", &connProfile, nil, "root\\StandardCimv2")
	for _, r := range connProfile {
		fmt.Println("-- Connids: ", r.InterfaceAlias, r.InterfaceIndex, r.Name)
	}
}
