package main

import (
	"fmt"
	"time"

	"github.com/StackExchange/wmi"
	ole "github.com/go-ole/go-ole"
	"github.com/go-ole/go-ole/oleutil"
)

type Partial_PerfRawData struct {
	Name                 string
	PercentIdleTime      uint64
	PercentIdleTime_Base uint64
	//PercentDiskWriteTime uint64
}

func main() {
	wmiByLib()
}

var lastMap map[string]Partial_PerfRawData

func wmiByLib() {
	lastMap = make(map[string]Partial_PerfRawData)

	for i := 0; i < 30; i++ {
		wmiByLibOnce()
		time.Sleep(1 * time.Second)
	}
}

func wmiByLibOnce() {
	defer detectEllipsed(time.Now())

	var data []Partial_PerfRawData

	wmi.Query(
		"SELECT Name,PercentIdleTime,PercentIdleTime_Base FROM Win32_PerfRawData_PerfDisk_LogicalDisk",
		&data)
	for _, d := range data {

		last := lastMap[d.Name]

		diffIdle := (d.PercentIdleTime - last.PercentIdleTime)
		diffIdleBase := (d.PercentIdleTime_Base - last.PercentIdleTime_Base)
		fmt.Printf("%10s  %10d %10d  %3.2f\n",
			d.Name, diffIdle, diffIdleBase, 1.0-(float32(diffIdle)/float32(diffIdleBase)))
		lastMap[d.Name] = d
	}
}

func wmiByOleutil() {
	// init COM, oh yeah
	ole.CoInitialize(0)
	defer ole.CoUninitialize()

	unknown, _ := oleutil.CreateObject("WbemScripting.SWbemLocator")
	// unknown, _ := oleutil.CreateObject("WbemScripting.SWbemRefresher")
	defer unknown.Release()

	wmi, _ := unknown.QueryInterface(ole.IID_IDispatch)
	defer wmi.Release()

	// service is a SWbemServices
	serviceRaw, _ := oleutil.CallMethod(wmi, "ConnectServer")
	service := serviceRaw.ToIDispatch()
	defer service.Release()

	for i := 0; i < 10; i++ {
		detectOnce(service)
		time.Sleep(1 * time.Second)
	}
}

func detectEllipsed(start time.Time) {
	println("Ellipsed ", time.Since(start).String())
}

func detectOnce(service *ole.IDispatch) {
	defer detectEllipsed(time.Now())

	// result is a SWBemObjectSet
	//resultRaw, _ := oleutil.CallMethod(service, "ExecQuery", "SELECT * FROM Win32_PerfFormattedData_PerfDisk_LogicalDisk")
	resultRaw, _ := oleutil.CallMethod(service, "ExecQuery", "SELECT Name,PercentDiskTime,PercentDiskWriteTime FROM Win32_PerfRawData_PerfDisk_LogicalDisk WHERE Name='C:'")
	result := resultRaw.ToIDispatch()
	defer result.Release()

	countVar, _ := oleutil.GetProperty(result, "Count")
	count := int(countVar.Val)

	for i := 0; i < count; i++ {
		// item is a SWbemObject, but really a Win32_Process
		itemRaw, _ := oleutil.CallMethod(result, "ItemIndex", i)
		item := itemRaw.ToIDispatch()
		defer item.Release()

		fmt.Printf("%5s  %10s  %10s\n",
			getPropertyString(item, "Name"),
			getPropertyString(item, "PercentDiskTime"),
			getPropertyString(item, "PercentDiskWriteTime"))
	}
}

func getPropertyString(item *ole.IDispatch, name string) string {
	asString, _ := oleutil.GetProperty(item, name)
	return asString.ToString()
}
