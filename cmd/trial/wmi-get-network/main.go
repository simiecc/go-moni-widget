package main

import (
	"fmt"
	"time"

	"github.com/StackExchange/wmi"
	"gitlab.com/simiecc/go-moni-widget/data"
	"gitlab.com/simiecc/go-moni-widget/sysmetrics"
)

type NetworkStat = data.NetworkStat

var previous map[string]MSFT_NetAdapterStatisticsSettingData
var tickCount map[string]int

type MSFT_NetAdapterStatisticsSettingData = sysmetrics.MSFT_NetAdapterStatisticsSettingData

func main() {
	defer enterAlternative()()
	recvCh := make(chan []NetworkStat, 5)
	go startNetworkDetect(recvCh)

	for netData := range recvCh {
		// clearScreen()
		moveCursor(1, 1)

		for _, inf := range netData {
			fmt.Printf("\033[1;32m [ %s ] \033[m\n", inf.Name)

			recvColor := colorise(inf.RecvKBPerSec)
			sendColor := colorise(inf.SendKBPerSec)

			fmt.Printf("    ↓ %s%10s\033[m (%12.2f)", recvColor, format(inf.RecvKBPerSec), inf.RecvKBPerSec)
			fmt.Printf("    ↑ %s%10s\033[m (%12.2f)\n", sendColor, format(inf.SendKBPerSec), inf.SendKBPerSec)
		}
	}
}

func colorise(val float32) string {
	if val > 2000.0 {
		return "\033[1;31m"
	} else if val > 100.0 {
		return "\033[1;34m"
	}

	return "\033[1;37m"
}

func startNetworkDetect(recvCh chan []NetworkStat) {
	previous = make(map[string]data.Partial_Win32_PerfRawData_Tcpip_NetworkInterface)
	ticker := time.NewTicker(1 * time.Second)

	queryNetworkLoad()
	for {
		select {
		case <-ticker.C:
			recvCh <- queryNetworkLoad()
		}

	}
}

func queryNetworkLoad() []NetworkStat {
	var stats []NetworkStat

	queryString := "SELECT * FROM Win32_PerfRawData_Tcpip_NetworkInterface"
	var netRaw []Partial_Win32_PerfRawData_Tcpip_NetworkInterface
	wmi.Query(queryString, &netRaw)

	for _, n := range netRaw {
		prev, ok := previous[n.Name]
		if !ok {
			previous[n.Name] = n
			continue
		}
		previous[n.Name] = n

		// fmt.Printf("\033[1;32m [ %s ] \033[m - %s\n", n.Name)

		timeDiff := n.Timestamp_PerfTime - prev.Timestamp_PerfTime
		// fmt.Printf("    Timestamp = %20d (%6d)\n", n.Timestamp_PerfTime, timeDiff)

		timestampAsSec := float64(timeDiff) / 10000000.0
		moveCursor(9, 1)
		fmt.Printf("    Timestamp = %5.3f", timestampAsSec)

		// bandwidthKB := float32(float64(n.CurrentBandwidth) / KB)
		// fmt.Printf("    Curr BW   = %10.0f KB\n", bandwidthKB)

		recvDiff := n.BytesReceivedPersec - prev.BytesReceivedPersec
		recvKBPerSec := float32(float64(recvDiff) / KB / timestampAsSec)
		// recvPercent := (recvKBPerSec / bandwidthKB) * 100.0
		// fmt.Printf("    Receive   = %12d %12d (%s) %5.1f%%\n", n.BytesReceivedPersec, recvDiff, format(recvKBPerSec), recvPercent)

		sendDiff := n.BytesSentPersec - prev.BytesSentPersec
		sendKBPerSec := float32(float64(sendDiff) / KB / timestampAsSec)
		// sendPercent := (sendKBPerSec / bandwidthKB) * 100.0
		// fmt.Printf("    Send      = %12d %12d (%s) %5.1f%%\n", n.BytesSentPersec, sendDiff, format(sendKBPerSec), sendPercent)

		stats = append(stats, NetworkStat{
			Name:         n.Name,
			SendKBPerSec: sendKBPerSec,
			RecvKBPerSec: recvKBPerSec,
		})
	}
	return stats

}

func enterAlternative() func() {
	fmt.Print("\033[?1049h")
	return func() {
		fmt.Print("\033[?1049l")
	}
}

func clearScreen() {
	fmt.Print("\033[2J")
}

func moveCursor(row, column int) {
	//CSI n ; m H
	fmt.Printf("\033[%d;%dH", row, column)
}

const (
	KB = 1000.0
)

func format(kBPerSecv float32) string {
	if kBPerSecv < 2000.0 {
		return fmt.Sprintf("%7.1f KB", kBPerSecv)
	}

	mb := kBPerSecv / KB
	return fmt.Sprintf("%7.2f MB", mb)
}
