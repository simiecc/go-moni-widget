package main

import (
	"fmt"
	"time"

	"github.com/simie-cc/w32"
)

func main() {
	var powerStatus w32.SYSTEM_POWER_STATUS
	fmt.Println("Starting..")
	fmt.Print("\033[2J")

	for {
		time.Sleep(5 * time.Second)
		fmt.Print("\033[0;0H")

		if !w32.GetSystemPowerStatus(&powerStatus) {
			fmt.Println("error!")
			return
		}

		if powerStatus.BatteryFlag&0x1 == 0 {
			fmt.Println("No battery!")
			return
		}

		//"🔋" "🔌"
		if powerStatus.ACLineStatus == 0 {
			fmt.Print("Battery ")
		} else if powerStatus.BatteryFlag&0x8 > 0 {
			fmt.Print("Charging ")
		} else {
			continue
		}

		fmt.Println(powerStatus.BatteryLifePercent)

		fmt.Println("AC mode: ", powerStatus.ACLineStatus,

			", Battery: ", powerStatus.BatteryLifePercent)

	}
}
