module battery-level

go 1.14

require github.com/simie-cc/w32 v1.1.0

replace github.com/simie-cc/w32 => ../../../../w32
