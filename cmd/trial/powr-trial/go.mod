module gitlab.com/simiecc/go-moni-widget/powr-trial

require (
github.com/simie-cc/w32 v1.1.1-0.20200529045438-690424e76306
)

replace github.com/simie-cc/w32 => "../../w32"

go 1.14
