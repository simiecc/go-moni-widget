package main

import (
	"fmt"
	"time"
	"unsafe"

	"github.com/simie-cc/w32"
)

func main() {

	info := w32.GetSystemInfo()
	fmt.Println("GetSystemInfo dwNumberOfProcessors=", info.DwNumberOfProcessors)

	var powrInfo []w32.PROCESSOR_POWER_INFORMATION
	powrInfo = make([]w32.PROCESSOR_POWER_INFORMATION, info.DwNumberOfProcessors)

	for i := 0; i < 10; i++ {

		ret := w32.CallNtPowerInformation(w32.ProcessorInformation,
			nil, 0,
			unsafe.Pointer(&powrInfo[0]), unsafe.Sizeof(powrInfo)*uintptr(info.DwNumberOfProcessors))
		fmt.Println("sizeof ", unsafe.Sizeof(powrInfo))
		fmt.Printf("%X \n", ret)
		for index, info := range powrInfo {
			fmt.Println("info ", index, info.CurrentMhz)
		}
		time.Sleep(1 * time.Second)
	}
}
