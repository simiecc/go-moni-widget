package main

type dataSet struct {
	stats   Stat
	sem     chan bool
	maxSize int
	dirty   bool
}

func newDataSet(maxSize int) *dataSet {
	set := &dataSet{
		sem:     make(chan bool, 1),
		maxSize: maxSize + 1,
		dirty:   false,
		stats: Stat{
			CpuLoad: 0,
		},
	}
	return set
}

func initialStats(count int) []Stat {
	var ret []Stat
	for i := 0; i < count; i++ {
		ret = append(ret, Stat{
			CpuLoad: 0,
		})
	}
	return ret
}

func (d *dataSet) SetMaxSize(newMaxSize int) {
	d.maxSize = newMaxSize
}

func (d *dataSet) Append(newStat Stat) {
	defer d.lock()()

	// d.stats = append(d.stats, newStat)
	// d._trimStat()
	d.stats = newStat
	d.dirty = true
}

// func (d *dataSet) _trimStat() {
// 	sizeDiff := len(d.stats) - d.maxSize
// 	if sizeDiff > 0 {
// 		d.stats = d.stats[sizeDiff:]
// 	}
// }

func (d *dataSet) SetDirty() {
	d.dirty = true
}

func (d *dataSet) IsDirty() bool {
	return d.dirty
}

func (d *dataSet) Render(renderFunc func(Stat)) {
	defer d.lock()()

	if d.dirty {
		renderFunc(d.stats)
		d.dirty = false
	}
}

// Usage: `defer d.lock()()`
func (d *dataSet) lock() func() {
	d.sem <- true
	return func() { d.unlock() }
}
func (d *dataSet) unlock() {
	<-d.sem
}
