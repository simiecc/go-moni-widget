
SHELL = /bin/bash
BIN = $(CURDIR)/bin
VERSION := $(shell git describe --tags --always --dirty=X --match="v*" --abbrev=6 2> /dev/null || \
            echo v0)
BUILDTIME := $(shell date +"%Y%m%d-%H%M%S")
BUILDER := $(shell hostname)
BINARY_NAME = "moni-widget"
VERSION_FLAGS = \
	-X "main.Version=$(VERSION)" \
	-X "main.BuildTime=$(BUILDTIME)" \
	-X "main.Builder=$(BUILDER)"

all: build

clean:
	rm -rf $(BIN)

build: echo-version build-windows build-windows-release

$(BIN):
	mkdir $@

echo-version: $(BIN)
	@echo "$(VERSION)" > "$(BIN)/version"

# -H windowsgui
build-windows: $(BIN) $(BINARY_NAME).syso
	GOOS=windows go build -v \
        -ldflags ' $(VERSION_FLAGS)' \
        -o $(BIN)/$(BINARY_NAME).debug.exe
build-windows-release: $(BIN) $(BINARY_NAME).syso
	GOOS=windows go build -v \
        -tags release \
        -ldflags '-s -H windowsgui -X main.windowMode=true $(VERSION_FLAGS)' \
        -o $(BIN)/$(BINARY_NAME).exe

$(BINARY_NAME).syso: main.rc main.exe.manifest
	bash build-rc.sh $(BINARY_NAME)

bin/%.debug: $(BIN)
	echo "== $* (DEBUG) =="
	GOOS=windows go build -v \
		-ldflags ' $(VERSION_FLAGS)' \
		-o $(BIN)/$*.debug.exe ./cmd/$*

bin/%.release: $(BIN)
	echo "== $* (RELEASE) =="
	GOOS=windows go build -v \
		-tags release \
		-ldflags '-w -H windowsgui $(VERSION_FLAGS)' \
		-o $(BIN)/$*.exe ./cmd/$*
