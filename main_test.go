package main

import (
	"fmt"
	"log"
	"testing"
	"unsafe"

	"github.com/simie-cc/w32"
	"go.uber.org/zap"
)

func TestUintptr(t *testing.T) {
	var pos w32.WINDOWPOS
	pos.X = 20

	var ptr uintptr = uintptr(unsafe.Pointer(&pos))
	posafter := (*w32.WINDOWPOS)(unsafe.Pointer(ptr))
	fmt.Println(posafter)
}

func TestZap(t *testing.T) {
	logger, err := zap.NewDevelopment(zap.AddCaller())
	if err != nil {
		log.Fatalf("can't initialize zap logger: %v", err)
	}
	defer logger.Sync()

	logger.Info("Testing1234",
		zap.String("url", "http://www.google.com"))
}
