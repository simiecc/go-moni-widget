package main

import (
	"unsafe"

	"github.com/simie-cc/w32"
	"gitlab.com/simiecc/golib/clog"
)

func registerAppBarNotify(hWnd w32.HWND) {
	var barData w32.APPBARDATA
	barData.CbSize = unsafe.Sizeof(barData)
	barData.HWnd = hWnd
	barData.UCallbackMessage = APPBAR_CALLBACK

	ret := w32.SHAppBarMessage(w32.ABM_NEW, &barData)
	clog.Info("SHAppBarMessage ABM_NEW ret ", ret)
}

func unregisterAppBarNotify(hWnd w32.HWND) {
	var barData w32.APPBARDATA
	barData.CbSize = unsafe.Sizeof(barData)
	barData.HWnd = hWnd
	// barData.UCallbackMessage = APPBAR_CALLBACK

	ret := w32.SHAppBarMessage(w32.ABM_REMOVE, &barData)
	clog.Info("SHAppBarMessage ABM_REMOVE ret ", ret)
}

func appBarCallback(_ w32.HWND, _ uint32, wParam, lParam uintptr) uintptr {
	clog.Debug("Appbar callback ", wParam, lParam)
	switch wParam {
	case w32.ABN_FULLSCREENAPP:
		var showMsg string
		if lParam == 1 {
			showMsg = "SHOW"
			showMainWindow(false)
		} else {
			showMsg = "HIDE"
			showMainWindow(true)
		}
		clog.Info("ABN_FULLSCREENAPP ", showMsg)
	}
	return 0
}
