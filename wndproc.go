package main

import (
	"time"
	"unsafe"

	"github.com/simie-cc/w32"
	"gitlab.com/simiecc/go-moni-widget/config"
	"gitlab.com/simiecc/go-moni-widget/data"
	"gitlab.com/simiecc/go-moni-widget/renderer3"
	"gitlab.com/simiecc/go-moni-widget/sysmetrics"
	"gitlab.com/simiecc/go-moni-widget/utils"
	"gitlab.com/simiecc/golib/clog"
)

var renderer data.Renderer
var forceUpdateChan chan bool
var updateRequested = false
var quitUpdate chan bool
var windowMoving bool = false
var windowMovingX, windowMovingY int

// WndProc main message process function
func WndProc(hWnd w32.HWND, msg uint32, wParam, lParam uintptr) uintptr {
	//fmt.Printf("Message: %v\n", msg)

	switch msg {
	case w32.WM_CREATE:
		hWndMain = hWnd
		mainWindowShown = true

		clog.Debugf("Message %v", "WM_CREATE")
		clog.Infof("Window created (%d)", hWnd)

		mainWindowDc = w32.GetDC(hWnd)
		rect := w32.GetClientRect(hWnd)
		//renderer = renderer2.NewDiagramRenderer(mainWindowDc, rect)
		renderer = renderer3.NewDiagramRenderer(mainWindowDc, rect)

		renderer.RenderBitmap()
		renderer.RenderToDC(mainWindowDc)
		//width := renderer.CalculateWidth()

		taskbarCreatedMsg = w32.RegisterWindowMessage("TaskbarCreated")
		clog.Info("RegisterWindowMessage(TaskbarCreated) ret=", taskbarCreatedMsg)

		// w32.SetWindowPos(hWnd, w32.HWND_TOPMOST, 0, 0, int(width), int(rect.Bottom),
		// 	uint(w32.SWP_NOACTIVATE|w32.SWP_NOMOVE))
		quitUpdate = startPeriodicUpdate()

		registerAppBarNotify(hWnd)
		registerSessionChangeNotification(hWnd)

	case w32.WM_PAINT:
		clog.Debugf("Message %v", "WM_PAINT")
		drawWindow()

	case w32.WM_NCLBUTTONDBLCLK:
		clog.Debugf("Message %v", "WM_NCLBUTTONDBLCLK")
		SwitchWindowInteractable()

	case w32.WM_LBUTTONDBLCLK:
		clog.Debugf("Message %v", "WM_LBUTTONDBLCLK")
		SwitchWindowInteractable()

	case w32.WM_DESTROY:
		unregisterAppBarNotify(hWnd)

		quitUpdate <- true
		renderer.OnDestroy()
		clog.Info("Message WM_DESTROY")
		DestroyMenu()
		RemoveTrayIcon()

		w32.PostQuitMessage(0)

	case WM_USER_NOTIFY:
		handleNotify(msg, wParam, lParam)

	case APPBAR_CALLBACK:
		appBarCallback(hWnd, msg, wParam, lParam)

		// case w32.WM_MOVE:
		// 	clog.Debug("Message", "WM_MOVE")

		// case w32.WM_SIZE:
		// 	clog.Debug("Message", "WM_SIZE")
		// 	rect := w32.GetClientRect(hWnd)
		// 	renderer.SetRect(rect)

	case w32.WM_MOUSEHOVER:
		clog.Debug("WM_MOUSEHOVER")

	case w32.WM_LBUTTONDOWN:
		windowMoving = true
		windowMovingX, windowMovingY = cursorPosLparam(lParam)
		clog.Debugf("Start moving (%d,%d)", windowMovingX, windowMovingY)
		w32.SetCapture(hWnd)

	case w32.WM_LBUTTONUP:
		windowMoving = false
		clog.Debugf("End moving")
		w32.ReleaseCapture()

	case w32.WM_MOUSEMOVE:
		if !windowMoving {
			return 0
		}

		lButtonDown := utils.Has(wParam, 0x0001 /* w32.MK_LBUTTON */)
		if !lButtonDown {
			clog.Debug("Detected Move but not Lbutton")
			UpdateWindowInteractable(false)
			return 0
		}

		// clog.Debugf("lParam %#x", lParam)
		currX, currY := cursorPosLparam(lParam)
		clog.Debugf("WM_MOUSEMOVE (%d,%d)", currX, currY)

		diffX := currX - windowMovingX
		diffY := currY - windowMovingY

		rect := w32.GetWindowRect(hWnd)
		rect.Left += int32(diffX)
		rect.Top += int32(diffY)
		rect.Right += int32(diffX)
		rect.Bottom += int32(diffY)

		hmon := w32.MonitorFromPoint(rect.Left, rect.Top, w32.MONITOR_DEFAULTTONEAREST)
		var mi w32.MONITORINFO
		mi.CbSize = uint32(unsafe.Sizeof(mi))
		w32.GetMonitorInfo(hmon, &mi)

		mdiffX, mdiffY := checkMonitorAttach(rect, mi)
		if mdiffX != 0 || mdiffY != 0 {
			clog.Debugf("Monitor attach (%d,%d)", mdiffX, mdiffY)
			rect.Left += int32(mdiffX)
			rect.Top += int32(mdiffY)
			rect.Right += int32(mdiffX)
			rect.Bottom += int32(mdiffY)
		}

		w32.SetWindowPos(hWnd, 0, int(rect.Left), int(rect.Top), 0, 0, w32.SWP_NOSIZE|w32.SWP_NOZORDER)

	// case w32.WM_NCHITTEST:
	// 	// Call the default window procedure for default handling.
	// 	result := w32.DefWindowProc(hWnd, msg, wParam, lParam)

	// 	// You want to change HTCLIENT into HTCAPTION.
	// 	// Everything else should be left alone.
	// 	if result == w32.HTCLIENT {
	// 		return w32.HTCAPTION
	// 	}

	// 	return result

	case w32.WM_WTSSESSION_CHANGE:
		clog.Debugf("Message %v %v", "WM_WTSSESSION_CHANGE", wParam)
		if wParam == w32.WTS_SESSION_UNLOCK {
			setWindowTopPos()
		}

	// case w32.WM_WINDOWPOSCHANGING:
	// 	winpos := (*w32.WINDOWPOS)(unsafe.Pointer(lParam))
	// 	clog.Debugf("Message %v %v %v", "WM_WINDOWPOSCHANGING", winpos.X, winpos.Y)

	case w32.WM_WINDOWPOSCHANGED:
		winpos := (*w32.WINDOWPOS)(unsafe.Pointer(lParam))
		clog.Debugf("Message %v %v %v", "WM_WINDOWPOSCHANGED", winpos.X, winpos.Y)
		setWindowTopPos()

		posSame := int(winpos.X) == config.CurrentConfig.X && int(winpos.Y) == config.CurrentConfig.Y
		if posSame {
			return 0
		}

		config.CurrentConfig.X = int(winpos.X)
		config.CurrentConfig.Y = int(winpos.Y)

		if !updateRequested {
			updateRequested = true
			go func() {
				time.Sleep(10 * time.Second)
				w32.SendMessage(hWnd, WM_USER_NOTIFY_SAVECONFIG, 0, 0)
			}()
		}

	case WM_USER_NOTIFY_SAVECONFIG:
		clog.Debug("Message ", "WM_USER_NOTIFY_SAVECONFIG")
		updateRequested = false
		config.CurrentConfig.SaveConfig(CONFIG_FILENAME)

	case w32.WM_COMMAND:
		return handleCommand(msg, wParam, lParam)

	case w32.WM_DISPLAYCHANGE:
		height := lParam >> 16
		width := lParam & 0xFFFF
		clog.Debug("Message ", "WM_DISPLAYCHANGE ", "height=", height, "width=", width)

	case w32.WM_POWERBROADCAST:
		if wParam == w32.PBT_APMPOWERSTATUSCHANGE {
			clog.Debug("Message ", "WM_POWERBROADCAST ", "PBT_APMPOWERSTATUSCHANGE")
			updateCh <- sysmetrics.UpdatePower
		}

	case taskbarCreatedMsg:
		clog.Info("Message ", "(Registered) TaskbarCreated")
		addTrayIcon(true)

	case w32.WM_DEVICECHANGE:
		clog.Debug("Message ", "WM_DEVICECHANGE ", wParam, lParam)
		updateCh <- sysmetrics.UpdateDisk

	case WM_USER_NOTIFY_LOCCHANGE:
		clog.Debug("Message ", "WM_USER_NOTIFY_LOCCHANGE")
		updateCh <- sysmetrics.UpdateLocation
	}

	return w32.DefWindowProc(hWnd, msg, wParam, lParam)
}

func handleNotify(msg uint32, wParam, lParam uintptr) {
	switch lParam {
	case w32.WM_CONTEXTMENU:
		clog.Debug("Message ", "User WM_CONTEXTMENU")

	case w32.WM_RBUTTONUP:
		clog.Debug("Message ", "User WM_LBUTTONUP", wParam)

		x, y, ok := w32.GetCursorPos()
		if !ok {
			clog.Error("GetCursorPos failed.")
			return
		}
		PopupMenu(int(x), int(y))

	case w32.WM_LBUTTONUP:
		clog.Debug("Message ", "User WM_RBUTTONUP")
		setWindowTopPos()
	}
}

func handleCommand(msg uint32, wParam, lParam uintptr) uintptr {
	clog.Debug("Message ", "WM_COMMAND", wParam, lParam)
	switch wParam {
	case MENUID_SWITCH_TRANSPARENT:
		SwitchWindowInteractable()

	case MENUID_QUIT:
		w32.PostMessage(hWndMain, w32.WM_CLOSE, 0, 0)

	default:
		return w32.DefWindowProc(hWndMain, msg, wParam, lParam)
	}

	return 0
}

func drawWindow() {
	var ps w32.PAINTSTRUCT
	hdc := w32.BeginPaint(hWndMain, &ps)
	defer w32.EndPaint(hWndMain, &ps)

	drawWindow0(hdc, false)
}

func drawWindow0(targetDC w32.HDC, forceUpdate bool) {
	if !mainWindowShown {
		return
	}

	if forceUpdate && !datas.IsDirty() {
		renderer.RenderBitmap()

	} else {
		datas.Render(func(stat Stat) {
			renderer.UpdateStat(stat)
			checkWindowSize()
			renderer.RenderBitmap()
		})
	}

	renderer.RenderToDC(targetDC)
}

func checkWindowSize() {
	rect := w32.GetClientRect(hWndMain)
	width := int32(renderer.Width())
	height := int32(renderer.Height())

	if rect.Right != width || rect.Bottom != height {
		clog.Info("Resize window to ", width, "x", height)

		w32.SetWindowPos(hWndMain, w32.HWND_TOPMOST, 0, 0, int(width), int(height),
			uint(w32.SWP_NOACTIVATE|w32.SWP_NOMOVE|w32.SWP_ASYNCWINDOWPOS))
	}
}

func cursorPosLparam(lParam uintptr) (x, y int) {
	rawX := w32.LOWORD(uint32(lParam))
	x = checkNegative(rawX)
	rawY := w32.HIWORD(uint32(lParam))
	y = checkNegative(rawY)
	return
}

func checkNegative(rawVal uint16) int {
	if rawVal>>15 == 0 {
		return int(rawVal)
	}
	return -int(^rawVal) + 1
}

const attachThres = 20

func checkMonitorAttach(rect *w32.RECT, mi w32.MONITORINFO) (diffX, diffY int32) {
	diffX, diffY = 0, 0
	if absInt32(rect.Left-mi.RcWork.Left) <= attachThres {
		diffX = -(rect.Left - mi.RcWork.Left)
	} else if absInt32(rect.Right-mi.RcWork.Right) <= attachThres {
		diffX = -(rect.Right - mi.RcWork.Right)
	}

	if absInt32(rect.Top-mi.RcWork.Top) <= attachThres {
		diffY = -(rect.Top - mi.RcWork.Top)
	} else if absInt32(rect.Bottom-mi.RcWork.Bottom) <= attachThres {
		diffY = -(rect.Bottom - mi.RcWork.Bottom)
	}
	return
}

func absInt32(val int32) int32 {
	if val < 0 {
		return -val
	}
	return val
}
