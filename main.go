package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"regexp"
	"runtime"
	"runtime/debug"
	"strings"

	"github.com/marcsauter/single"
	"github.com/simie-cc/w32"
	"gitlab.com/simiecc/go-moni-widget/config"
	"gitlab.com/simiecc/go-moni-widget/traceByHttp"
	"gitlab.com/simiecc/golib/clog"
	"go.uber.org/zap"
)

// Version program version
var Version = "DEV"

// Builder is hostname of build machine
var Builder = ""

const CONFIG_FILENAME = "moni-widget.json"

var windowMode = "false"
var taskbarCreatedMsg uint32

func init() {
	config.InitProgramArgs()

	if windowMode == "true" {
		dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
		if err != nil {
			log.Fatal(err)
		}

		outputPath := filepath.Join(dir, "moni-widget.log")
		outputPath = strings.Replace(outputPath, "\\", "/", -1)
		outputPath = regexp.MustCompile("[A-Z]:\\/").ReplaceAllString(outputPath, "/")
		clog.Info("Log write to", outputPath)
		clog.Replace(clog.NewProductionConfig(outputPath, func(config *zap.Config) {
			config.Level = zap.NewAtomicLevelAt(zap.InfoLevel)
		}))
	}

	initialControlC()

	clog.Info("PID:", os.Getpid())
}

func versionStr() string {
	var atBuilder string
	if len(Builder) > 0 {
		atBuilder = "@" + Builder
	}
	return fmt.Sprintf("%s, %s%s", Version, runtime.Version(), atBuilder)
}

func initialControlC() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	go func() {
		for sig := range c {
			// sig is a ^C, handle it
			clog.Info("Received", sig)

			if config.IsDebug() {
				var b = make([]byte, 10240)
				runtime.Stack(b, true)
				clog.Error(strings.Trim(string(b), " \x00"))
			}

			w32.PostMessage(hWndMain, w32.WM_CLOSE, 0, 0)
		}
	}()
}

func main() {
	singleLock := single.New("go-moni-widget")
	if err := singleLock.CheckLock(); err != nil && err == single.ErrAlreadyRunning {
		clog.Fatal("Already running, exiting.")
	} else if err != nil {
		// Another error occurred, might be worth handling it as well
		clog.Fatalf("failed to acquire exclusive app lock: %v", err)
	}
	defer singleLock.TryUnlock()

	defer func() {
		if r := recover(); r != nil {
			clog.Error("Panic: ", r)
			clog.Fatal("stacktrace from panic: \n" + string(debug.Stack()))
		}
	}()

	traceByHttp.StartTraceHttp(context.Background(), "127.0.0.1:38876")
	WinMain()
}
