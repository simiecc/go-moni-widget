package sysmetrics

import (
	"github.com/simie-cc/w32"
	"gitlab.com/simiecc/go-moni-widget/data"
	"gitlab.com/simiecc/golib/clog"
)

type batteryDetector struct {
	detector
}

func newBatteryDetector() *batteryDetector {
	return &batteryDetector{}
}

func (b *batteryDetector) Detect(stat *Stat) {
	var powerStatus w32.SYSTEM_POWER_STATUS
	if w32.GetSystemPowerStatus(&powerStatus) {
		flagNoSystemBattery := (powerStatus.BatteryFlag & 0x80) > 0
		batteryStat := data.BatteryStat{
			Shown:        !flagNoSystemBattery,
			AcLineStatus: powerStatus.ACLineStatus,
			Charging:     (powerStatus.BatteryFlag & 0x8) > 0,
			Level:        int(powerStatus.BatteryLifePercent),
		}

		clog.Debug("BatteryShown: ", batteryStat.Shown, "(flag:", powerStatus.BatteryFlag, ")",
			" AcLineStatus: ", batteryStat.AcLineStatus,
			" Charging: ", batteryStat.Charging,
			" BatteryLevel: ", batteryStat.Level)
		stat.Battery = batteryStat
		return

	} else {
		stat.Battery = data.BatteryStat{
			Shown: false,
		}
		return
	}
}
