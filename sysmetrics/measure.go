package sysmetrics

import (
	"time"

	"gitlab.com/simiecc/golib/clog"
)

type MeasureEllapsed struct {
	SysMetrics

	wrapped    SysMetrics
	spentInfos []SpentInfo
	level      int
}

type SpentInfo struct {
	Name      string
	TimeMills time.Duration
	Level     int
}

func NewMeasureEllapsed(impl SysMetrics) *MeasureEllapsed {
	return &MeasureEllapsed{
		wrapped: impl,
		level:   0,
	}
}

func (mea *MeasureEllapsed) GetCurrentMetrics(forcePowerUpdate, forceDiskUpdate, forceUpdateNetwork bool) Stat {
	// defer mea.spentTime("GetCurrentMetrics")()
	// defer mea.outputSpent()

	var currentStat Stat

	// // Detect Disk changes every 1 min
	// if forceDiskUpdate {
	// 	mea.detectSystemDisks(&currentStat)
	// }

	// if forcePowerUpdate {
	// 	mea.detectPowerStatus(&currentStat)
	// } else {
	// 	currentStat.Battery = mea.getLastBatteryStat()
	// }

	// mea.getCPULoad(&currentStat)
	// mea.getMemoryLoad(&currentStat)
	// mea.getDiskLoad(&currentStat)

	// if forceUpdateNetwork {
	// 	mea.getNetworkLoad(&currentStat)
	// } else {
	// 	currentStat.NetworkLoad = mea.getLastNetworkLoad()
	// }

	return currentStat
}

func (mea *MeasureEllapsed) detectSystemDisks(stat *Stat) {
	// defer mea.spentTime("detectSystemDisks")()
	// mea.wrapped.detectSystemDisks(stat)
}

// func (mea *MeasureEllapsed) detectPowerStatus(stat *Stat) {
// 	defer mea.spentTime("detectPowerStatus")()
// 	mea.wrapped.detectPowerStatus(stat)
// }
// func (mea *MeasureEllapsed) getLastBatteryStat() data.BatteryStat {
// 	defer mea.spentTime("getLastBatteryStat")()
// 	return mea.wrapped.getLastBatteryStat()
// }
// func (mea *MeasureEllapsed) getLastNetworkLoad() []data.NetworkStat {
// 	defer mea.spentTime("getLastNetworkLoad")()
// 	return mea.wrapped.getLastNetworkLoad()
// }
func (mea *MeasureEllapsed) getCPULoad(stat *Stat) {
	// defer mea.spentTime("getCPULoad")()
	// mea.wrapped.getCPULoad(stat)
}
func (mea *MeasureEllapsed) getMemoryLoad(stat *Stat) {
	// defer mea.spentTime("getMemoryLoad")()
	// mea.wrapped.getMemoryLoad(stat)
}
func (mea *MeasureEllapsed) getDiskLoad(stat *Stat) {
	// defer mea.spentTime("getDiskLoad")()
	// mea.wrapped.getDiskLoad(stat)
}

// func (mea *MeasureEllapsed) getNetworkLoad(stat *Stat) {
// 	defer mea.spentTime("getNetworkLoad")()
// 	mea.wrapped.getNetworkLoad(stat)
// }

// func spentTime(start time.Time) {
// 	if config.IsDebug() {
// 		duration := time.Since(start)
// 		perfCounters = append(perfCounters, duration.Milliseconds())
// 	}
// }

func (mea *MeasureEllapsed) spentTime(functionName string) func() {
	start := time.Now()
	mea.level++

	return func() {
		mea.level--
		mea.spentInfos = append(mea.spentInfos, SpentInfo{
			Name:      functionName,
			TimeMills: time.Since(start),
			Level:     mea.level,
		})
	}
}

func (mea *MeasureEllapsed) outputSpent() {
	clog.Info("Ellapsed Info:")
	for _, info := range mea.spentInfos {
		clog.Infof("  %*s %s : %3d ms", 3+info.Level*2, "| ", info.Name, info.TimeMills.Milliseconds())
	}

	mea.spentInfos = mea.spentInfos[:0]
}
