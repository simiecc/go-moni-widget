package sysmetrics

import (
	"fmt"
	"strings"

	"github.com/StackExchange/wmi"
	"github.com/simie-cc/w32"
	"gitlab.com/simiecc/golib/clog"
)

type diskLoadContext struct {
	diskCounts     DiskCounts
	prevDiskSpaces map[string]DiskStat
}

func newDiskLoadContext() *diskLoadContext {
	return &diskLoadContext{
		prevDiskSpaces: make(map[string]DiskStat),
	}
}

type systemDisksDetector struct {
	detector
	ctx *diskLoadContext
}

func newSystemDisksDetector(ctx *diskLoadContext) *systemDisksDetector {
	return &systemDisksDetector{
		ctx: ctx,
	}
}

func (s *systemDisksDetector) Detect(stat *Stat) {
	wmi.Query("SELECT DeviceID FROM Win32_LogicalDisk WHERE size > 0", &s.ctx.diskCounts.Disks)

	strb := strings.Builder{}
	strb.WriteString("SELECT Name,PercentIdleTime,PercentIdleTime_Base FROM Win32_PerfRawData_PerfDisk_LogicalDisk WHERE")
	for idx, d := range s.ctx.diskCounts.Disks {
		if idx > 0 {
			strb.WriteString(" OR")
		}
		strb.WriteString(fmt.Sprintf(" Name='%v'", d.DeviceID))

		r, _, totalBytes, freeBytes := w32.GetDiskFreeSpaceEx(d.DeviceID)
		if r {
			clog.Debugf("Free space of %v = %d / %d", d.DeviceID, freeBytes, totalBytes)
			s.ctx.prevDiskSpaces[d.DeviceID] = DiskStat{
				TotalBytes: totalBytes,
				FreeBytes:  freeBytes,
			}
		}

	}
	s.ctx.diskCounts.QueryString = strb.String()
}

type diskLoadDetector struct {
	detector
	ctx *diskLoadContext

	prevDiskStats map[string]Partial_Win32_PerfRawData_PerfDisk_LogicalDisk
}

func newDiskLoadDetector(ctx *diskLoadContext) *diskLoadDetector {
	return &diskLoadDetector{
		ctx:           ctx,
		prevDiskStats: make(map[string]Partial_Win32_PerfRawData_PerfDisk_LogicalDisk),
	}
}

func (d *diskLoadDetector) Detect(stat *Stat) {
	var diskRaw []Partial_Win32_PerfRawData_PerfDisk_LogicalDisk
	wmi.Query(d.ctx.diskCounts.QueryString, &diskRaw)

	var outputStat []DiskStat
	for _, raw := range diskRaw {
		prevStat := d.prevDiskStats[raw.Name]
		prevSpace := d.ctx.prevDiskSpaces[raw.Name]
		newStat := DiskStat{
			Name: raw.Name,
			BuzyPercent: 100.0 -
				(float64(raw.PercentIdleTime-prevStat.PercentIdleTime)/float64(raw.PercentIdleTime_Base-prevStat.PercentIdleTime_Base))*100.0,
			TotalBytes: prevSpace.TotalBytes,
			FreeBytes:  prevSpace.FreeBytes,
		}
		if newStat.BuzyPercent > 100 {
			newStat.BuzyPercent = 100
		}
		if newStat.BuzyPercent < 0 {
			newStat.BuzyPercent = 0
		}
		outputStat = append(outputStat, newStat)

		d.prevDiskStats[raw.Name] = raw
	}
	stat.DiskLoad = outputStat
}
