package sysmetrics

import (
	"time"

	"gitlab.com/simiecc/go-moni-widget/data"
	"gitlab.com/simiecc/golib/clog"
)

type Stat = data.Stat
type DiskStat = data.DiskStat
type NetworkStat = data.NetworkStat

var netUpdateDefault = 1

type ForceUpdateEvent int

const (
	UpdatePower ForceUpdateEvent = iota
	UpdateDisk
	UpdateNetwork
	UpdateLocation
)

// StartLoadDetect starting load detection in a new goroutine
func StartLoadDetect(ticker *time.Ticker, updateCh chan ForceUpdateEvent) chan Stat {
	deliverChan := make(chan Stat, 10)

	// Build first record of detection record
	clog.Info("Initializing load detection...")
	metrics := SysMetrics(NewSysMetricsImpl())
	// TODO permonitor should be re-written
	// if config.IsPerfMon() {
	// 	metrics = NewMeasureEllapsed(metrics)
	// }

	startNetworkChangeDetect(updateCh)

	go func() {
		// 0. Initial phase
		deliverChan <- metrics.GetCurrentMetrics(true, true, true, true)

		// 1. Running phase
		clog.Debug("StartLoadDetect()")
		var forcePowerUpdate = false
		var forceDiskUpdate = false
		var forceNetUpdate = false
		var forceLocationUpdate = false

		for {
			select {
			case <-ticker.C:
				{
					stat := metrics.GetCurrentMetrics(
						forcePowerUpdate,
						forceDiskUpdate,
						forceNetUpdate,
						forceLocationUpdate,
					)

					deliverChan <- stat
					forcePowerUpdate = false
					forceDiskUpdate = false
					forceNetUpdate = false
					forceLocationUpdate = false
				}

			case item := <-updateCh:
				switch item {
				case UpdatePower:
					forcePowerUpdate = true
				case UpdateDisk:
					forceDiskUpdate = true
				case UpdateNetwork:
					forceNetUpdate = true
				case UpdateLocation:
					forceLocationUpdate = true
				}
			}
		}
	}()

	return deliverChan
}

type SysMetrics interface {
	GetCurrentMetrics(forcePowerUpdate, forceDiskUpdate, forceUpdateNetwork, forceLocation bool) Stat
}

type detector interface {
	Detect(stat *Stat)
}

type periodicDetector interface {
	detector
	SetForceUpdate()
}

type periodicDetectorImpl struct {
	periodicDetector

	impl          detector
	getUpdateTick func(stat *Stat) int
	tick          int
}

func newPeriodicDetector(d detector, getUpdateTick func(stat *Stat) int) periodicDetector {
	return &periodicDetectorImpl{
		impl:          d,
		getUpdateTick: getUpdateTick,
	}
}

func (p *periodicDetectorImpl) Detect(stat *Stat) {
	p.tick--
	if p.tick > 0 {
		return
	}

	p.impl.Detect(stat)
	p.tick = p.getUpdateTick(stat)
	// clog.Debug("Updates ", reflect.TypeOf(p.impl), " next tick ", p.tick)
}

func (p *periodicDetectorImpl) SetForceUpdate() {
	p.tick = 0

	implPeriod, ok := p.impl.(periodicDetector)
	if ok {
		implPeriod.SetForceUpdate()
	}
}
