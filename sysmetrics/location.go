package sysmetrics

import (
	"io/ioutil"
	"os"
	"path/filepath"

	"gitlab.com/simiecc/golib/clog"
)

type locationDetector struct {
	detector
}

func newLocationDetector() *locationDetector {
	return &locationDetector{}
}

func (d *locationDetector) Detect(stat *Stat) {
	homeDir, err := os.UserHomeDir()
	if err != nil {
		stat.Location = "err: home unknown"
		clog.Error("err: home unknown: ", err)
		return
	}

	locfile := filepath.Join(homeDir, ".location")

	file, err := os.OpenFile(locfile, os.O_RDONLY, 0644)
	if err != nil {
		stat.Location = ""
		clog.Error("err: open failed: ", err)
		return
	}
	defer file.Close()

	data, err := ioutil.ReadAll(file)
	if err != nil {
		stat.Location = "err: read error"
		clog.Error("err: read error: ", err)
		return
	}

	stat.Location = string(data)
	clog.Debug("Current Location: ", stat.Location)
}
