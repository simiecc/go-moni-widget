package sysmetrics

import (
	"github.com/StackExchange/wmi"
	"gitlab.com/simiecc/golib/clog"
)

const (
	KB = 1000.0

	QuietCountDefault = 20
)

type NetRawData struct {
	WmiData    MSFT_NetAdapterStatisticsSettingData
	QuietCount int8
	Timenano   int64
}

type SysMetricsImpl struct {
	SysMetrics

	detectors []detector

	latestStat Stat

	diskDetector     periodicDetector
	batteryDetector  periodicDetector
	networkDetector  periodicDetector
	locationDetector periodicDetector

	networkRawDetector *networkDetector
}

func NewSysMetricsImpl() *SysMetricsImpl {

	clog.Debugf("Initializing SWbemServices")
	s, err := wmi.InitializeSWbemServices(wmi.DefaultClient)
	if err != nil {
		clog.Fatal(err)
	}
	wmi.DefaultClient.SWbemServicesClient = s

	imp := &SysMetricsImpl{}
	diskCtx := newDiskLoadContext()
	imp.diskDetector = newPeriodicDetector(
		newSystemDisksDetector(diskCtx),
		func(_ *Stat) int { return 300 },
	)
	imp.batteryDetector = newPeriodicDetector(
		newBatteryDetector(),
		func(stat *Stat) int {
			battery := stat.Battery
			if !battery.Charging && battery.AcLineStatus == 1 {
				return 600 // TODO meaning..?
			} else {
				return 120
			}
		},
	)
	imp.networkRawDetector = newNetworkDetector()
	imp.networkDetector = newPeriodicDetector(
		imp.networkRawDetector,
		func(stat *Stat) int {
			if stat.Battery.AcLineStatus == 1 {
				return 1
			} else {
				return 3
			}
		},
	)
	imp.locationDetector = newPeriodicDetector(
		newLocationDetector(),
		func(stat *Stat) int { return 86400 })

	imp.detectors = []detector{
		imp.batteryDetector,
		newCpuLoadDetector(),
		newMemLoadDetector(),
		imp.diskDetector,
		newDiskLoadDetector(diskCtx),
		imp.networkDetector,
		imp.locationDetector,
	}

	//imp.GetCurrentMetrics(true, true, true)
	return imp
}

func (imp *SysMetricsImpl) GetCurrentMetrics(forcePowerUpdate, forceDiskUpdate, forceUpdateNetwork, forceLocation bool) Stat {

	var currentStat = imp.latestStat

	if forceDiskUpdate {
		imp.diskDetector.SetForceUpdate()
	}

	if forcePowerUpdate {
		imp.batteryDetector.SetForceUpdate()
	}

	if forceUpdateNetwork {
		imp.networkRawDetector.SetForceUpdate()
		imp.networkDetector.SetForceUpdate()
	}

	if forceLocation {
		imp.locationDetector.SetForceUpdate()
	}

	for i := range imp.detectors {
		imp.detectors[i].Detect(&currentStat)
	}

	imp.latestStat = currentStat
	return currentStat
}
