package sysmetrics_test

import "testing"

var ticks = []int{1, 5, 1, 60, 1}

func BenchmarkByCounter(b *testing.B) {
	tickCounter := make([]int, len(ticks))
	for i := 0; i < b.N; i++ {
		countDown(tickCounter)
	}
}

func countDown(counter []int) {
	for i, _ := range counter {
		counter[i]--
		if counter[i] < 0 {
			// call update
			counter[i] = ticks[i]
		}
	}
}

func BenchmarkByMod(b *testing.B) {
	for i := 0; i < b.N; i++ {
		for _, m := range ticks {
			checkByMod(i, m)
		}
	}
}

func checkByMod(i, m int) bool {
	return i%m == 0
}
