package sysmetrics

import (
	"fmt"
	"testing"
	"time"
)

func TestCpuLoad(t *testing.T) {
	d := newCpuLoadDetector()

	for i := 0; i < 10; i++ {
		var stat Stat
		d.Detect(&stat)
		fmt.Printf("Percent %v %v\n",
			stat.CpuLoad, stat.CpuLoadKernel)
		//		fmt.Println()

		time.Sleep(1 * time.Second)
	}
}
