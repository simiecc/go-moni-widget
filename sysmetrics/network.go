package sysmetrics

import (
	"time"

	"github.com/StackExchange/wmi"
	"github.com/simie-cc/w32"
	"gitlab.com/simiecc/golib/clog"
)

type netAdapter struct {
	AliasName      string
	InterfaceIndex uint32
}

type networkDetector struct {
	ssidUpdate   bool
	netNameCache map[string]MSFT_NetAdapter
	netSsidCache map[uint32]string
	prevNetStats map[string]*NetRawData
}

func newNetworkDetector() *networkDetector {
	return &networkDetector{
		prevNetStats: make(map[string]*NetRawData),
		netNameCache: make(map[string]MSFT_NetAdapter),
		netSsidCache: make(map[uint32]string),
		ssidUpdate:   true,
	}
}

func (nd *networkDetector) Detect(stat *Stat) {
	if nd.ssidUpdate {
		nd.updateNetworkAdapter()
		nd.updateSsid()
		nd.ssidUpdate = false
	}

	var netStats []NetworkStat

	var netRaw []MSFT_NetAdapterStatisticsSettingData
	wmi.Query(
		"SELECT * FROM MSFT_NetAdapterStatisticsSettingData",
		&netRaw,
		nil, "root\\StandardCimv2")

	for _, n := range netRaw {
		prev, ok := nd.prevNetStats[n.Name]
		if !ok {
			nd.prevNetStats[n.Name] = &NetRawData{
				WmiData:    n,
				QuietCount: 0,
			}
			continue
		}

		timenano := time.Now().UnixNano()
		prveTimenano := prev.Timenano
		prev.Timenano = timenano

		prevWmiData := prev.WmiData
		prev.WmiData = n
		// fmt.Printf("\033[1;32m [ %s ] \033[m - %s\n", n.Name)

		// timestampAsSec := float64(timeDiff) / 10000000.0
		// fmt.Printf("    Timestamp = %20d (%6d)\n", n.Timestamp_PerfTime, timeDiff)

		if (prevWmiData.ReceivedBytes > n.ReceivedBytes) ||
			(prevWmiData.SentBytes > n.SentBytes) {
			continue
		}

		recvDiff := n.ReceivedBytes - prevWmiData.ReceivedBytes
		sendDiff := n.SentBytes - prevWmiData.SentBytes
		// fmt.Println("n.BytesReceivedPersec=", n.BytesReceivedPersec, "(", recvDiff, ")")

		if recvDiff > 0 || sendDiff > 0 {
			prev.QuietCount = QuietCountDefault
		}

		if prev.QuietCount > 0 {
			prev.QuietCount--

			timeDiff := float64(timenano-prveTimenano) / 1000000000.0 // as seconds
			// clog.Infof("Intf %s Send %d Recv %d (Time diff %f)", n.Name, sendDiff, recvDiff, timeDiff)
			recvKBPerSec := float64(recvDiff) / KB / timeDiff
			sendKBPerSec := float64(sendDiff) / KB / timeDiff
			adt := nd.getNetworkAdapter(n.Name)
			var ssid string
			if adt.InterfaceType == 71 {
				ssid = nd.getSsid(adt.InterfaceIndex)
			}

			netStats = append(netStats, NetworkStat{
				Name:         n.Name,
				Ssid:         ssid,
				SendKBPerSec: sendKBPerSec,
				RecvKBPerSec: recvKBPerSec,
			})
		}
	}

	stat.NetworkLoad = netStats
}

func (nd *networkDetector) getSsid(interfaceIndex uint32) string {
	ssid, ok := nd.netSsidCache[interfaceIndex]
	if !ok {
		return ""
	}
	return ssid
}

func (nd *networkDetector) updateSsid() {
	cache := make(map[uint32]string)
	var connProfile []Partial_MSFT_NetConnectionProfile
	wmi.Query("SELECT * FROM MSFT_NetConnectionProfile", &connProfile, nil, "root\\StandardCimv2")
	for _, r := range connProfile {
		clog.Debugf("SSID for %d = %s", r.InterfaceIndex, r.Name)
		cache[r.InterfaceIndex] = r.Name
		//fmt.Println("-- Connids: ", r.InterfaceAlias, r.InterfaceIndex, r.Name)
	}
	nd.netSsidCache = cache
}

func (nd *networkDetector) getNetworkAdapter(name string) MSFT_NetAdapter {
	adt, ok := nd.netNameCache[name]
	if ok {
		return adt
	}

	return MSFT_NetAdapter{
		Name:           name,
		InterfaceIndex: 0,
	}
}

func (nd *networkDetector) updateNetworkAdapter() {

	// clog.Debug("Net formated name=", formatedName)

	var resultRaw []MSFT_NetAdapter
	wmi.Query(
		"SELECT * FROM MSFT_NetAdapter",
		&resultRaw,
		nil, "root\\StandardCimv2")

	cache := make(map[string]MSFT_NetAdapter)
	for _, result := range resultRaw {
		cache[result.Name] = result
		clog.Debugf("NetAdapter \"%s\" = [%d] (InterfaceType %d)", result.Name,
			result.InterfaceIndex, result.InterfaceType)
	}

	nd.netNameCache = cache
}

func (nd *networkDetector) SetForceUpdate() {
	nd.ssidUpdate = true
}

func startNetworkChangeDetect(updateCh chan ForceUpdateEvent) {
	go func() {
		for {
			w32.NotifyAddrChange(nil, nil)
			updateCh <- UpdateNetwork
		}
	}()
}
