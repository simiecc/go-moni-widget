package sysmetrics

import "github.com/simie-cc/w32"

func fileTimeToInt64(ft w32.FILETIME) uint64 {
	return ((uint64(ft.DwHighDateTime) << 32) | uint64(ft.DwLowDateTime))
}

func byteToMB(byteVal uint64) uint64 {
	return byteVal / 1024 / 1024
}
