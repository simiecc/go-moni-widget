package sysmetrics

import (
	"unsafe"

	"github.com/simie-cc/w32"
	"gitlab.com/simiecc/golib/clog"
)

type cpuLoadDetector struct {
	detector

	previousKernelTicks uint64
	previousUserTicks   uint64
	previousIdleTicks   uint64
}

func newCpuLoadDetector() *cpuLoadDetector {
	return &cpuLoadDetector{
		previousKernelTicks: 0,
		previousUserTicks:   0,
		previousIdleTicks:   0,
	}
}

func (c *cpuLoadDetector) Detect(stat *Stat) {
	var idleTime, kernelTime, userTime w32.FILETIME
	ret := w32.GetSystemTimes(&idleTime, &kernelTime, &userTime)
	if !ret {
		clog.Error("Call GetSystemTimes() failed.")
		return
	}
	//      fmt.Println("ret", ret)
	//      fmt.Println("idleTime", fileTimeToInt64(idleTime))
	//      fmt.Println("kernelTime", fileTimeToInt64(kernelTime))
	//      fmt.Println("userTime", fileTimeToInt64(userTime))
	tIdle := fileTimeToInt64(idleTime)
	tKernel := fileTimeToInt64(kernelTime)
	tUser := fileTimeToInt64(userTime)
	loadTotal, loadKernel := c.calculateCPULoad(tIdle, tKernel, tUser)

	//	fmt.Printf("Percent %.2f %v %v\n",
	//		load, t1, t2)
	//      fmt.Println()
	stat.CpuLoad = loadTotal
	stat.CpuLoadKernel = loadKernel
}

func (c *cpuLoadDetector) calculateCPULoad(idleTicks, kernelTicks, userTicks uint64) (loadTotal, loadKernel int) {

	kernelTicksDraft := calculateDraft(kernelTicks, &c.previousKernelTicks)
	userTicksDraft := calculateDraft(userTicks, &c.previousUserTicks)
	idleTicksDraft := calculateDraft(idleTicks, &c.previousIdleTicks)

	totalTicksDraft := (kernelTicksDraft + userTicksDraft)
	if totalTicksDraft > 0 {
		loadTotal = 100 - int(idleTicksDraft*100/totalTicksDraft)
		loadKernel = 100 - int((idleTicksDraft+userTicksDraft)*100/totalTicksDraft)
	} else {
		loadTotal = 100
		loadKernel = 0
	}

	return
}

func calculateDraft(currentValue uint64, previousValue *uint64) uint64 {
	draft := currentValue - *previousValue
	*previousValue = currentValue
	return draft
}

type memLoadDetector struct {
	detector
}

func newMemLoadDetector() *memLoadDetector {
	return &memLoadDetector{}
}

func (c *memLoadDetector) Detect(stat *Stat) {
	var mstat w32.MEMORYSTATUSEX
	mstat.Length = w32.DWORD(unsafe.Sizeof(mstat))
	ret := w32.GlobalMemoryStatusEx(&mstat)
	if !ret {
		clog.Error("Call GlobalMemoryStatusEx() failed.")
		return
	}

	stat.MemoryTotalInMB = byteToMB(mstat.TotalPhys)
	stat.MemoryUsedInMB = byteToMB(mstat.TotalPhys - mstat.AvailPhys)
	stat.MemoryLoad = int(stat.MemoryUsedInMB * 100.0 / stat.MemoryTotalInMB)
}
