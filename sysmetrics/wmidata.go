package sysmetrics

type Partial_PerfFormattedData struct {
	Name                 string
	PercentDiskTime      uint64
	PercentDiskWriteTime uint64
}

type Partial_Win32_PerfRawData_PerfDisk_LogicalDisk struct {
	Name                 string
	PercentIdleTime      uint64
	PercentIdleTime_Base uint64
}

type Partial_Win32_PerfRawData_Tcpip_NetworkAdapter struct {
	Name string
	// Caption string
	// Description         string

	// Timestamp_PerfTime  uint64
	Timestamp_Sys100NS  uint64
	BytesReceivedPersec uint64
	BytesSentPersec     uint64
	// BytesTotalPersec uint64
	// CurrentBandwidth uint64

	// Frequency_PerfTime uint64
}

// https://docs.microsoft.com/en-us/previous-versions/windows/desktop/legacy/hh968170(v=vs.85)
type MSFT_NetAdapter struct {
	Name           string
	InterfaceIndex uint32
	// InterfaceDescription string
	InterfaceType uint32
	// NdisPhysicalMedium   uint32
}

type Partial_LogicalDisk struct {
	DeviceID string
}

type DiskCounts struct {
	Disks       []Partial_LogicalDisk
	QueryString string
}

type Partial_MSFT_NetConnectionProfile struct {
	Name           string
	InterfaceIndex uint32

	//InterfaceAlias string
	// Caption	CIM_ManagedElement	8 [string]
	// Description	CIM_ManagedElement	8 [string]
	// ElementName	CIM_ManagedElement	8 [string]
	// InstanceID	CIM_ManagedElement	8 [string]
	// IPv4Connectivity	MSFT_NetConnectionProfile	19 [uint32]
	// IPv6Connectivity	MSFT_NetConnectionProfile	19 [uint32]

	// NetworkCategory	MSFT_NetConnectionProfile	19 [uint32]
}

// https://docs.microsoft.com/en-us/previous-versions/windows/desktop/netadaptercimprov/msft-netadapterstatisticssettingdata
type MSFT_NetAdapterStatisticsSettingData struct {
	// string Caption;
	// string Description;
	// string ElementName;
	// string InstanceID;
	Name string
	// string InterfaceDescription;
	// uint32 Source;
	// string SystemName;
	// uint32 SupportedStatistics;
	ReceivedBytes uint64
	// uint64 ReceivedUnicastPackets;
	// uint64 ReceivedMulticastPackets;
	// uint64 ReceivedBroadcastPackets;
	// uint64 ReceivedUnicastBytes;
	// uint64 ReceivedMulticastBytes;
	// uint64 ReceivedBroadcastBytes;
	// uint64 ReceivedDiscardedPackets;
	// uint64 ReceivedPacketErrors;
	SentBytes uint64
	// uint64 SentUnicastPackets;
	// uint64 SentMulticastPackets;
	// uint64 SentBroadcastPackets;
	// uint64 SentUnicastBytes;
	// uint64 SentMulticastBytes;
	// uint64 SentBroadcastBytes;
	// uint64 OutboundDiscardedPackets;
	// uint64 OutboundPacketErrors;
	// string RdmaStatistics;
	// string RscStatistics;
}
