package main

import (
	"math"
)

func multiplyUint64(val uint64, plyer float32) uint64 {
	return uint64(math.Round(float64(val) * float64(plyer)))
}

func maxUint64(val1, val2 uint64) uint64 {
	if val1 > val2 {
		return val1
	} else if val2 > val1 {
		return val2
	} else {
		return val1
	}
}

func maxFloat32(val1, val2 float32) float32 {
	if val1 > val2 {
		return val1
	} else if val2 > val1 {
		return val2
	} else {
		return val1
	}
}

func dontcare(val, threshold uint64) uint64 {
	if val <= threshold {
		return 0
	}
	return val
}
