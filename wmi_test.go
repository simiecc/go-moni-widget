package main

import (
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/StackExchange/wmi"
)

func ignore_TestGetData(t *testing.T) {
	var dstGetMemoryUsageMB []Win32_PerfRawData_PerfProc_Process
	filterProcessID := fmt.Sprintf("WHERE IDProcess = %d", os.Getpid())
	qGetMemoryUsageMB := wmi.CreateQuery(&dstGetMemoryUsageMB, filterProcessID)
	fmt.Println("result==>", qGetMemoryUsageMB)

	wmi.Query(qGetMemoryUsageMB, &dstGetMemoryUsageMB)
	fmt.Println("result2==>", dstGetMemoryUsageMB[0].WorkingSetPrivate)
}

type Win32_PerfRawData_PerfProc_Process struct {
	IDProcess         uint32
	WorkingSetPrivate uint64
}

func TestGetDiskPerf_Happycase(t *testing.T) {
	for i := 1; i <= 10; i++ {
		var datalist []Win32_PerfFormattedData_PerfDisk_PhysicalDisk_1
		err := wmi.Query("SELECT PercentDiskTime, PercentDiskWriteTime FROM Win32_PerfFormattedData_PerfDisk_PhysicalDisk WHERE Name <> '_Total'", &datalist)
		if err != nil {
			t.Error("Error", err)
		}

		for index, data := range datalist {
			fmt.Printf("[%d]%3d/%3d ", index,
				data.PercentDiskWriteTime, data.PercentDiskTime)
		}

		fmt.Printf("\n")

		time.Sleep(500 * time.Millisecond)
	}

	fmt.Printf("\n")
}

type Win32_PerfFormattedData_PerfDisk_PhysicalDisk_1 struct {
	PercentDiskTime      uint64
	PercentDiskWriteTime uint64
}

type Win32_PerfFormattedData_PerfDisk_PhysicalDisk struct {
	AvgDiskBytesPerRead     uint64
	AvgDiskBytesPerTransfer uint64
	AvgDiskBytesPerWrite    uint64
	AvgDiskQueueLength      uint64
	AvgDiskReadQueueLength  uint64
	AvgDiskSecPerRead       uint32
	AvgDiskSecPerTransfer   uint32
	AvgDiskSecPerWrite      uint32
	AvgDiskWriteQueueLength uint64
	Caption                 string
	CurrentDiskQueueLength  uint32
	Description             string
	DiskBytesPerSec         uint64
	DiskReadBytesPerSec     uint64
	DiskReadsPerSec         uint32
	DiskTransfersPerSec     uint32
	DiskWriteBytesPerSec    uint64
	DiskWritesPerSec        uint32
	Frequency_Object        uint64
	Frequency_PerfTime      uint64
	Frequency_Sys100NS      uint64
	Name                    string
	PercentDiskReadTime     uint64
	PercentDiskTime         uint64
	PercentDiskWriteTime    uint64
	PercentIdleTime         uint64
	SplitIOPerSec           uint32
	Timestamp_Object        uint64
	Timestamp_PerfTime      uint64
	Timestamp_Sys100NS      uint64
}

type Win32_LogicalDisk struct {
	DeviceID  string
	DriveType uint32

	Size uint64
}

func TestGetWin32_LogicalDisk_Happycase(t *testing.T) {
	var datalist []Win32_LogicalDisk
	err := wmi.Query("SELECT DeviceID, DriveType, Size FROM Win32_LogicalDisk", &datalist)
	if err != nil {
		t.Error("Error", err)
	}

	for _, data := range datalist {
		fmt.Println("Data", data.DeviceID, data.DriveType, data.Size)
	}
}
