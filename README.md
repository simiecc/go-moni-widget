
# **Go-moni-widget**

A small widget for monitoring CPU load/Memory Usage/Disk usage for WINDOWS. 

<img src='demo.gif' width='231'/>

# Releases

[Release](https://simiecc.gitlab.io/go-moni-widget/)

## developing install

- install msys2
- open msys2 terminal and run `pacman -S mingw-w64-x86_64-toolchain`
- set PATH add `C:\msys64\mingw64\bin`
- make clean pkger all

## Released under MIT
