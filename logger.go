package main

import (
	"log"

	"go.uber.org/zap"
)

var logger *zap.SugaredLogger = initLogger()

func initFileLogger(filePath string) *zap.SugaredLogger {
	config := zap.NewDevelopmentConfig()
	config.OutputPaths = []string{filePath}
	rawlogger, err := config.Build(zap.AddCaller())
	if err != nil {
		log.Fatalf("can't initialize zap logger: %v", err)
	}

	slogger := rawlogger.Sugar()

	return slogger
}

func initLogger() *zap.SugaredLogger {

	rawlogger, err := zap.NewDevelopment(zap.AddCaller())
	if err != nil {
		log.Fatalf("can't initialize zap logger: %v", err)
	}

	slogger := rawlogger.Sugar()

	//logger = rawlogger.Sugar()
	//slogger.Debug("Logger inited.")

	return slogger
	// TODO Watch IsDebug to provide different log
}
