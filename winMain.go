package main

import (
	"os"
	"runtime"
	"syscall"

	"unsafe"

	"github.com/simie-cc/w32"
	"gitlab.com/simiecc/go-moni-widget/config"
	"gitlab.com/simiecc/golib/clog"
)

const (
	WM_USER_NOTIFY            = w32.WM_USER + 1
	WM_USER_NOTIFY_SAVECONFIG = w32.WM_USER + 2
	WM_USER_NOTIFY_LOCCHANGE  = w32.WM_USER + 3
	APPBAR_CALLBACK           = w32.WM_USER + 10
)
const MENUID_SWITCH_TRANSPARENT = 20
const MENUID_QUIT = 21

const (
	DEFAULT_WINDOW_WIDTH  = 100
	DEFAULT_WINDOW_HEIGHT = 110
)

var hInstance w32.HINSTANCE
var hWndMain w32.HWND
var mainWindowShown bool
var mainWindowDc w32.HDC
var hMenu w32.HMENU

func WinMain() int {
	runtime.LockOSThread()

	defer clog.Sync()
	clog.Info("Opening widget window...")
	defer clog.Info("Quit WinMain()")

	w32.SetProcessDpiAwarenessContext(w32.DPI_AWARENESS_CONTEXT_PER_MONITOR_AWARE)

	err := config.CurrentConfig.LoadConfig(CONFIG_FILENAME)
	if err != nil {
		clog.Error("Config load failed: %v", err)
	}
	clog.Debugf("Config loaded: %#v", config.CurrentConfig)

	hInstance = w32.GetModuleHandle("")
	lpszClassName, err := syscall.UTF16PtrFromString("CCMoniWidgetClass")
	if err != nil {
		clog.Error("Format string error!", err)
		os.Exit(2)
	}

	// // Initialize GDI+.
	// var gdiplusStartupInput w32.GdiplusStartupInput
	// gdiplusStartupInput.GdiplusVersion = 1
	// w32.GdiplusStartup(&gdiplusStartupInput, nil)
	// defer w32.GdiplusShutdown()

	// // Load bittmap
	// ptr, err := w32.GdipCreateBitmapFromResource(hInstance, MakeIntResource(102))
	// if err != nil {
	// 	panic(err)
	// }
	// clog.Info("Pointer ", ptr)

	var wcex w32.WNDCLASSEX
	wcex.Size = uint32(unsafe.Sizeof(wcex))
	wcex.Style = w32.CS_HREDRAW | w32.CS_VREDRAW | w32.CS_DBLCLKS
	wcex.WndProc = syscall.NewCallback(WndProc)
	wcex.ClsExtra = 0
	wcex.WndExtra = 0
	wcex.Instance = hInstance
	wcex.Icon = w32.LoadIcon(hInstance, MakeIntResource(w32.IDI_APPLICATION))
	wcex.Cursor = w32.LoadCursor(0, MakeIntResource(w32.IDC_ARROW))
	wcex.Background = prepareTransparentBrush() //0 // w32.COLOR_WINDOW + 11
	wcex.MenuName = nil

	wcex.ClassName = lpszClassName
	wcex.IconSm = w32.LoadIcon(hInstance, MakeIntResource(w32.IDI_APPLICATION))
	w32.RegisterClassEx(&wcex)

	winExStyle := uint(w32.WS_EX_TOPMOST | w32.WS_EX_LAYERED | w32.WS_EX_TOOLWINDOW)
	// if !IsDebug() {
	// 	winExStyle |= w32.WS_EX_TRANSPARENT
	// }

	hWndMain = w32.CreateWindowEx(
		winExStyle,
		lpszClassName,
		syscall.StringToUTF16Ptr("Moni Widget"),
		w32.WS_VISIBLE|w32.WS_SYSMENU|w32.WS_POPUP,
		config.CurrentConfig.X, config.CurrentConfig.Y, DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT,
		0, 0, hInstance, nil)

	w32.ShowWindow(hWndMain, w32.SW_SHOWDEFAULT)
	w32.UpdateWindow(hWndMain)
	UpdateWindowInteractable(config.CurrentConfig.Interactable)

	addTrayIcon(false)

	var msg w32.MSG
	for {
		ret := w32.GetMessage(&msg, 0, 0, 0)
		if ret == 0 {
			break
		} else if ret == -1 {
			clog.Error("GetMessageError!")
			os.Exit(1)
		}

		w32.TranslateMessage(&msg)
		w32.DispatchMessage(&msg)
	}

	return int(msg.WParam)
}

func SwitchWindowInteractable() {
	config.CurrentConfig.Interactable = !config.CurrentConfig.Interactable
	UpdateWindowInteractable(config.CurrentConfig.Interactable)
}

func UpdateWindowInteractable(interactable bool) {
	config.CurrentConfig.Interactable = interactable

	exStyle := w32.GetWindowLongPtr(hWndMain, w32.GWL_EXSTYLE)
	if !interactable {
		exStyle |= w32.WS_EX_TRANSPARENT
		w32.SetWindowLongPtr(hWndMain, w32.GWL_EXSTYLE, exStyle)
		setWindowTransparent(true)
		setWindowTopPos()
		clog.Info("Window is now transparent.")
	} else {
		exStyle &^= w32.WS_EX_TRANSPARENT
		w32.SetWindowLongPtr(hWndMain, w32.GWL_EXSTYLE, exStyle)
		setWindowTransparent(false)
		setWindowTopPos()
		clog.Info("Window is now interactable.")
	}

	forceUpdateChan <- true
}

func showMainWindow(show bool) {
	if show {
		w32.ShowWindow(hWndMain, w32.SW_SHOWDEFAULT)
	} else {
		w32.ShowWindow(hWndMain, w32.SW_HIDE)
	}
	mainWindowShown = show
}

func setWindowTopPos() {
	w32.SetWindowPos(hWndMain, w32.HWND_TOPMOST, 0, 0, 0, 0,
		uint(w32.SWP_NOACTIVATE|w32.SWP_NOMOVE|w32.SWP_NOSIZE|w32.SWP_SHOWWINDOW))
}

func prepareTransparentBrush() w32.HBRUSH {
	// TODO: maybe lacked transBrush here
	transBrush := w32.CreateSolidBrush(config.TransparentColor)
	return transBrush
}

// Transparent windows with click through
func setWindowTransparent(trans bool) {
	if trans {
		w32.SetLayeredWindowAttributes(hWndMain, config.TransparentColor, 140, w32.LWA_ALPHA|w32.LWA_COLORKEY)
	} else {
		w32.SetLayeredWindowAttributes(hWndMain, config.TransparentColor, 255, w32.LWA_ALPHA|w32.LWA_COLORKEY)
	}
}

func MakeIntResource(id uint16) *uint16 {
	return (*uint16)(unsafe.Pointer(uintptr(id)))
}

func addTrayIcon(readd bool) {

	var nd w32.NOTIFYICONDATA
	nd.CbSize = uint32(unsafe.Sizeof(nd))
	nd.HWnd = hWndMain
	nd.UID = 0
	copy(nd.SzTip[:], syscall.StringToUTF16("Go-moni-widget 小監控"))
	nd.HIcon = w32.LoadIcon(hInstance, MakeIntResource(101))
	nd.UFlags = w32.NIF_MESSAGE | w32.NIF_TIP | w32.NIF_ICON
	nd.UCallbackMessage = WM_USER_NOTIFY
	nd.UTimeoutOrVersion = 10

	if readd {
		RemoveTrayIcon()
	}

	ret := w32.Shell_NotifyIcon(w32.NIM_ADD, &nd)
	if !ret {
		clog.Error("Add icon failed.")
	}

	createMenuForTrayIcon()

	// nd.UVersion = w32.NOTIFYICON_VERSION
	// ret = w32.Shell_NotifyIcon(w32.NIM_SETVERSION, &nd)
	// if !ret {
	// 	logger.Error("Error: Set verion")
	// }
}

func RemoveTrayIcon() {
	var nd w32.NOTIFYICONDATA
	nd.CbSize = uint32(unsafe.Sizeof(nd))
	nd.HWnd = hWndMain
	nd.UID = 0

	ret := w32.Shell_NotifyIcon(w32.NIM_DELETE, &nd)
	if !ret {
		clog.Error("RemoveTrayIcon failed.")
	}
}

func createMenuForTrayIcon() {
	hMenu = w32.CreatePopupMenu()
	insertDisabledMenuItem(1, "Go-moni-widget")
	insertDisabledMenuItem(1, "    ver "+versionStr())
	insertMenuItem(2, MENUID_SWITCH_TRANSPARENT, "切換透明")
	insertMenuItem(3, MENUID_QUIT, "離開")
}

func insertMenuItem(sequence uint32, commandID uint32, text string) {
	var mii w32.MENUITEMINFO
	mii.CbSize = uint32(unsafe.Sizeof(mii))
	mii.FMask = uint32(w32.MIIM_ID | w32.MIIM_STRING | w32.MIIM_DATA)
	mii.WID = commandID
	mii.DwTypeData = syscall.StringToUTF16Ptr(text)
	mii.Cch = uint32(len(text))

	ret := w32.InsertMenuItem(hMenu, sequence, true, &mii)
	if !ret {
		clog.Error("Insert Menu item failed.")
	}
}

func insertDisabledMenuItem(sequence uint32, text string) {
	var mii w32.MENUITEMINFO
	mii.CbSize = uint32(unsafe.Sizeof(mii))
	mii.FMask = uint32(w32.MIIM_STRING | w32.MIIM_DATA | w32.MIIM_STATE)
	mii.FState = uint32(w32.MFS_DISABLED)
	mii.DwTypeData = syscall.StringToUTF16Ptr(text)
	mii.Cch = uint32(len(text))

	ret := w32.InsertMenuItem(hMenu, sequence, true, &mii)
	if !ret {
		clog.Error("Insert Menu item failed.")
	}
}

func PopupMenu(x, y int) {
	w32.SetForegroundWindow(hWndMain)
	ret := w32.TrackPopupMenu(hMenu,
		w32.TPM_RIGHTALIGN|w32.TPM_LEFTBUTTON,
		x, y, 0, hWndMain, nil)
	if !ret {
		clog.Error("TrackPopupMenu failed.")
	}
}

func DestroyMenu() {
	ret := w32.DestroyMenu(hMenu)
	if !ret {
		clog.Error("DestroyMenu failed.")
	}
	hMenu = 0
}

func registerSessionChangeNotification(hWnd w32.HWND) {
	w32.WTSRegisterSessionNotification(hWnd, w32.NOTIFY_FOR_THIS_SESSION)
}
