package main

import (
	"runtime"
	"time"

	"gitlab.com/simiecc/go-moni-widget/config"
	"gitlab.com/simiecc/go-moni-widget/demoMode"
	"gitlab.com/simiecc/go-moni-widget/sysmetrics"
	"gitlab.com/simiecc/golib/clog"
)

var datas *dataSet
var updateCh chan sysmetrics.ForceUpdateEvent

func startPeriodicUpdate() chan bool {
	ticker := time.NewTicker(1000 * time.Millisecond)
	updateCh = make(chan sysmetrics.ForceUpdateEvent, 5)
	var statChan chan Stat
	if config.IsDemoMode() {
		clog.Info("Run in DEMO mode.")
		statChan = demoMode.StartLoadDetect(ticker)
	} else {
		clog.Info("Run in NORMAL mode.")
		statChan = sysmetrics.StartLoadDetect(ticker, updateCh)
	}
	//var stats []PStat = initialStats(renderer.MaxHistorySize())
	datas = newDataSet(renderer.MaxHistorySize())

	forceUpdateChan = make(chan bool, 1)
	quit := make(chan bool, 1)

	go func() {
		runtime.LockOSThread()
		for {
			select {
			case stat := <-statChan:
				datas.SetMaxSize(renderer.MaxHistorySize())
				datas.Append(stat)
				drawWindow0(mainWindowDc, false)

			case <-forceUpdateChan:
				drawWindow0(mainWindowDc, true)

			case <-quit:
				return
			}
		}
	}()

	return quit
}
