module gitlab.com/simiecc/go-moni-widget

require (
	github.com/StackExchange/wmi v0.0.0-20190523213315-cbe66965904d
	github.com/gin-gonic/gin v1.6.3
	github.com/marcsauter/single v0.0.0-20201009143647-9f8d81240be2
	github.com/simie-cc/w32 v1.1.11
	gitlab.com/simiecc/golib/clog v0.0.0-20200813011930-4f1f1bac900d
	go.uber.org/zap v1.16.0
	golang.org/x/sys v0.0.0-20210421221651-33663a62ff08
)

require (
	github.com/gin-contrib/sse v0.1.0 // indirect
	github.com/go-ole/go-ole v1.2.5 // indirect
	github.com/go-playground/locales v0.13.0 // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator/v10 v10.2.0 // indirect
	github.com/golang/protobuf v1.3.3 // indirect
	github.com/json-iterator/go v1.1.9 // indirect
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v0.0.0-20180701023420-4b7aa43c6742 // indirect
	github.com/ugorji/go/codec v1.1.7 // indirect
	go.uber.org/atomic v1.6.0 // indirect
	go.uber.org/multierr v1.5.0 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)

go 1.17
